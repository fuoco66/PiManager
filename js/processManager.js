var operationQueqe = [];
var CacheProcess = null;

$( document ).ready(function() {

  $( '#processManager' ).on( "show.bs.dropdown",  function( event ) {
    if(operationQueqe.length > 0){
      return;
    }
    $("#pM_processList").find(".template_procListItem").remove();;
    var processList = ['transmission', 'apache', 'samba', 'dummy'];
    GetProcessData(processList);
    
  });
    
  var templateUrl = "https://" + serverBaseUrl + serverRootPath + 'templates/';
  $("#procTemplate").load(templateUrl + "processManager.php .template_procListItem", function( response, status, xhr ) {

    if ( status == "error" ) {
      var msg = "Sorry but there was an error: ";
      $( "#error" ).html( msg + xhr.status + " " + xhr.statusText );
    }

   });


});

function GetProcessData(processList){

  var dataJSON = {
    action: "get_process_status",
    value: processList
  }

  $("#processManager_loader").addClass('waiting_ajax');

  $.ajax({
    type: "POST",
    url: "https://" + serverBaseUrl + serverRootPath + "/API/WS_System.php",
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {
      if( msg.code == 0 ) {
        CacheProcess = msg.response;
        RenderProcess(msg.response);
        
      } else {

        console.log("ERROR");
        $("#processManager_loader").removeClass('waiting_ajax');

      }
    },
    error: function(msg) {
      console.log("FATAL_ERROR");
      $("#processManager_loader").removeClass('waiting_ajax');

    },
    complete: function() {
    }
  });


}

function RenderProcess(processData){

  var procTemplate = $('#procTemplate').html();
  var ProcessContainer = $("#pM_processList");
  
  ProcessContainer.find(".template_procListItem").remove();

  for( i = 0; i < processData.length; i++ ) {
    CacheProcess[i]['localId'] = i;
    processData[i]['localId'] = i;
    var currentProcess = processData[i];

    currentProcess.statusClass = currentProcess.running == true ? 'btn-success' : 'btn-danger';
    
    currentProcess.buttonsStatus = {
      'start': currentProcess.running != true && currentProcess.enabledFunctions.start === 1 ? '' : 'disabled',
      'stop': currentProcess.running == true && currentProcess.enabledFunctions.stop === 1 ? '' : 'disabled',
      'restart': currentProcess.running == true && currentProcess.enabledFunctions.restart === 1 ? '' : 'disabled'
    }

    var output = Mustache.render(procTemplate, currentProcess);
    ProcessContainer.append(output);

    if(i < processData.length-1){
      ProcessContainer.append('<li class="divider template_procListItem"></li>');
    }

  }

  $('.pM-actionButton').unbind;
  $('button.pM-actionButton').click(function (e) {
      var localId = e.currentTarget.getAttribute("data-localId");
      var action = e.currentTarget.getAttribute("data-action");

      ProcessAction(action, CacheProcess[localId]);

    });

  ProcessContainer.slideDown('fast');
  ProcessContainer = null;
  $("#processManager_loader").removeClass('waiting_ajax');

}

function UpdateSingleProcessUI(responseData, processLocalId, success) {
  // .removeClass('btn-success').removeClass('btn-danger').addClass('btn-warning');
  var startButton = $('button.pM-actionButton[data-localId=' + processLocalId + '][data-action="start"]');
  var stopButton = $('button.pM-actionButton[data-localId=' + processLocalId + '][data-action="stop"]');
  var restartButton = $('button.pM-actionButton[data-localId=' + processLocalId + '][data-action="restart"]');
  var statusIcon = $('button.pM-statusButton[data-localId=' + processLocalId + ']');

  //if there were no errors, update the cached data
  if(success) {
    CacheProcess[processLocalId] = responseData;
    CacheProcess[processLocalId]['localId'] = processLocalId;    
    CacheProcess[processLocalId]['statusClass'] = responseData.running == true ? 'btn-success' : 'btn-danger';
    CacheProcess[processLocalId]['buttonsStatus'] = {
      'start': responseData.running != true && responseData.enabledFunctions.start === 1 ? '' : 'disabled',
      'stop': responseData.running == true && responseData.enabledFunctions.stop === 1 ? '' : 'disabled',
      'restart': responseData.running == true && responseData.enabledFunctions.restart === 1 ? '' : 'disabled'
    }
  } else {
    $('strong.processErrorField[data-localId=' + processLocalId + ']').text(responseData);
    $('div.processErrorField[data-localId=' + processLocalId + ']').show();
  }

  var currentProcess = CacheProcess[processLocalId];

  // UI
  //startButton
  if(currentProcess.running != true && currentProcess.enabledFunctions.start === 1 ){
    startButton.removeClass('disabled');
  }

  //stopButton
  if(currentProcess.running == true && currentProcess.enabledFunctions.stop === 1 ){
    stopButton.removeClass('disabled');
  }

  //stopButton
  if(currentProcess.running == true && currentProcess.enabledFunctions.restart === 1 ){
    restartButton.removeClass('disabled');
  }

  if(currentProcess.running == true ){
    statusIcon.addClass('btn-success').removeClass('btn-warning');
  } else{
    statusIcon.addClass('btn-danger').removeClass('btn-warning');
  }


}

function ProcessAction(processAction, currentProcess) {

  $("#processManager_loader").addClass('waiting_ajax');
  $('button.pM-statusButton[data-localId=' + currentProcess.localId + ']').removeClass('btn-success').removeClass('btn-danger').addClass('btn-warning');
  $('button.pM-actionButton[data-localId=' + currentProcess.localId + ']').addClass('disabled');

  $('div.processErrorField[data-localId=' + currentProcess.localId + ']').slideUp();
  $('strong.processErrorField[data-localId=' + currentProcess.localId + ']').text('');

  operationQueqe.push(currentProcess.localId);

  var dataJSON = {
    action: "process_action",
    value: {
      function: processAction,
      processId: currentProcess.processId
    }
  }

  $.ajax({
    type: "POST",
    url: "https://" + serverBaseUrl + serverRootPath + "/API/WS_System.php",
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {

      if( msg.code == 0 ) {
        console.log('OK');
        UpdateSingleProcessUI(msg.response, currentProcess.localId, true);
      } else {
        UpdateSingleProcessUI(msg.response, currentProcess.localId, false);        
        console.log("ERROR");
      }

    },
    error: function(msg) {
      console.log("FATAL_ERROR");
      UpdateSingleProcessUI(msg.response, currentProcess.localId, false);   

    },
    complete: function() {
      var index = operationQueqe.indexOf(currentProcess.localId);

      if (index > -1) {
        operationQueqe.splice(index, 1);
      }

      if(operationQueqe.length <= 0){
        $("#processManager_loader").removeClass('waiting_ajax');
      }

    }
  });
}