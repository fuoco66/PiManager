function CountOcurrences(str, value) {
  var regExp = new RegExp(value, "gi");
  return (str.match(regExp) || []).length;
}

function ManageTableSorterPagerRender(table_id, items, pager){

	if(pager == null){
		pager = true;
	}

	if (items > 0) {
		$("#" + table_id).show();
		$("#" + table_id + "_pager").show();

		$("#" + table_id).trigger('destroyPager');

		$("#" + table_id).trigger("destroy");

		$.tablesorter.addParser({
		  id: 'jojobOrderColumns',
		  is: function() { return true; },
		  format: function(s, table, cell, cellIndex) {
				var strSpecificOrder = $(cell).attr('data-column-order');
				if(strSpecificOrder == null || strSpecificOrder == ''){
					return s;
				}

				return strSpecificOrder;
		  },
		  type: 'text'
		});
		$("#" + table_id).tablesorter({
			widthFixed: true,
			tabIndex         : false,
			cssAsc           : 'headerSortDown',
			cssDesc          : 'headerSortUp',
			cssHeader        : 'header',
			cssNoSort: 'header fa-sort',
			sortReset : false,
			dateFormat : "ddmmyyyy",

			widgets: ["zebra"],
			widgetOptions : {
	      zebra : [ "odd", "even" ] }

		});

		if(pager != false && items > 20){
      $("#" + table_id).tablesorterPager({
				container: $("#" + table_id + "_pager"),
				tabIndex : false,
				output: '{page:input}/ {totalPages} ',
				page: 0,
				savePages : false,
				pageReset: 0,
				cssNext: '.next',
				cssPrev: '.prev',
				cssFirst: '.first',
				cssLast: '.last',
				cssPageDisplay: '.pagedisplay',
				positionFixed: false
			});

			$("#" + table_id).trigger('pageAndSize', [1, 20]);
      // $("#" + table_id).trigger('pagerUpdate');

			$(".pageSizeInput").keydown(function (e) {
	        // Allow: backspace, delete, tab, escape, enter and .
	        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
	             // Allow: Ctrl+A, Command+A
	            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
	             // Allow: home, end, left, right, down, up
	            (e.keyCode >= 35 && e.keyCode <= 40)) {
	                 // let it happen, don't do anything
	                 return;
	        }
	        // Ensure that it is a number and stop the keypress
	        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	            e.preventDefault();
	        }
	    });

		}
		else {
			$("#" + table_id + "_pager").hide();
		}

		$("." + table_id + "_NoData").hide();

	}
	else{
		$("#" + table_id).hide();

		if(pager != false){
		$("#" + table_id + "_pager").hide(); }

		$("." + table_id + "_NoData").show(); }
}

function InitSystem(){

	var dataJSON = {
    action: "init_system",
		value: {
			forceDbLoad: true
		}
  }

  $.ajax({
    type: "POST",
    url: "https://" + serverBaseUrl + serverRootPath + "/API/WS_Util.php",
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {
      if( msg.code == 0 ) {
      } else {
        console.log(msg.code);
      }
    },
    error: function(msg) {
      
    },
    complete: function() {
    }
  });

}