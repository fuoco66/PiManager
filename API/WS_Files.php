<?php
  header('Content-type: application/json');

  require_once("ORM_Util.php");
  require_once("ORM_FilesManager.php");
 
  session_start();
  clearstatcache();

  $UtilObject = new ORM_Util();
  $filesManager = new ORM_FilesManager();

  $errorCode = 0;
  $errorMessage = "";
  $return_object = "OK";

  $mediaRoot = "/media/Data/Downloads";

  $JSON = $UtilObject->VerifyWsParameters($errorCode, $errorMessage);

  switch ($JSON->action) {

    case "list_files":

      $return_object = $filesManager->GetDirectoryObjects($JSON->path, $errorCode, $errorMessage);

      break;
    case "copy_object":

      $return_object = $filesManager->CopyObject($JSON->value->source, $JSON->value->destination, $errorCode, $errorMessage);

      break;
    case "copy_running":
      // $JSON->value->source, $JSON->value->destination,
      $return_object = $filesManager->GetCopyStatus(true, $errorCode, $errorMessage);

      break;
    case "copy_status":
      // $JSON->value->source, $JSON->value->destination,
      $return_object = $filesManager->GetCopyStatus(false, $errorCode, $errorMessage);

      break;
    case "copy_stop":

      $return_object = $filesManager->StopAllCopyTask($errorCode, $errorMessage);

      break;
     case "delete_files":
      // $return_object = "ciaone";
      $return_object = $filesManager->DeleteFiles($JSON->value->files, $errorCode, $errorMessage);

      break;
    default:

      $errorCode = -666;
      $errorMessage = "INVALID_REQUEST";

      break;

  }

  echo $UtilObject->ManageWebServiceResponse($return_object, $errorCode, $errorMessage);

?>
