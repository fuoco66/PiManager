#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

char* concat(const char *s1, const char *s2){
    char *result = malloc(strlen(s1)+strlen(s2)+1);//+1 for the zero-terminator
    //in real code you would check for errors in malloc here
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

int main (int argc, char *argv[]){
   setuid (0);
   char* s1 = concat("/bin/sh /var/www/vhosts/pimanu.net/public/piManager/fileManager/script/deleteFile.sh '", argv[1]);
   s1 = concat(s1, "'");

   return system ( s1 );
 }
