$( document ).ready(function() {

  $('a.advancedLink').on('click', function(e){

    var data = $(this).attr("data-deepLink").split("@");
    var destination = data[0];
    var info = data[1];

    if( $(location).attr('href').indexOf('fileManager') >= 0){
      ForwardDirectory = null;
      ForwardDirectory = [];
      LoadFileList(info);
      // console.log("change to " + destination + " -> " + info);
    } else {
      window.location.replace("/" + destination + "#" + info );
    }

  });

});
