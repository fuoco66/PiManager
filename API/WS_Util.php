<?php
  header('Content-type: application/json');

  require_once("ORM_Util.php");

  session_start();
  clearstatcache();

  $UtilObject = new ORM_Util();

  $errorCode = 0;
  $errorMessage = "";
  $return_object = "OK";

  $JSON = $UtilObject->VerifyWsParameters($errorCode, $errorMessage);

  switch ($JSON->action) {

    case "init_system":

      if(!isset($_SESSION["initSystem"]) || $_SESSION["initSystem"] == false || $JSON->value->forceDbLoad == true){
        $returnObject = $UtilObject->InitPiManager(true, $JSON->value->forceDbLoad, $errorCode, $errorMessage);
        $_SESSION["initSystem"] = true;
      } else{
        $returnObject =  $UtilObject->InitPiManager(false, $JSON->value->forceDbLoad, $errorCode, $errorMessage);        
      }
      

      break;
    default:

      $errorCode = -666;
      $errorMessage = "INVALID_REQUEST";

      break;

  }

  echo $UtilObject->ManageWebServiceResponse($return_object, $errorCode, $errorMessage);

?>
