#!/bin/sh

# working
echo RUNNING@@$1@@$2@@$3@@$3$1
sourceName=$1
sourcePath=$2
dest=$3

rsync_cmd="rsync -ahv --protect-args --progress"
cmdToRun="${rsync_cmd} '${sourcePath}' '${dest}${sourceName}'"
eval "${cmdToRun}"

if [ "$?" -eq "0" ]
then
  echo '\nOK'@@$1@@$2@@$3@@$3$1
else
	# removes tmp and incomplete files
  ls -nao $3 | fgrep ".$1." | awk '{$1=$2=$3=$4=$5=$6=$7=""; sub(/^  */,"", $0); print }' | while IFS= read -r FILE; do
    rm_cmd="rm"
    cmdToRun="${rm_cmd} '${dest}${FILE}'"
    # echo \n\nDELETED\n $cmdToRun
    eval "${cmdToRun}"
  done

  echo '\nERROR'@@$1@@$2@@$3@@$3$1
fi
