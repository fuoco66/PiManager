#!/bin/bash

# @param string $1
#   Input string.
# @param int $2
#   Cut an amount of characters from left side of string.
# @param int [$3]
#   Leave an amount of characters in the truncated string.
function substr {
    local length=${3}

    if [ -z "${length}" ]; then
        length=$((${#1} - ${2}))
    fi

    local str=${1:${2}:${length}}

    if [ "${#str}" -eq "${#1}" ]; then
        echo "${1}"
    else
        echo "${str}"
    fi
}

# @param string $1
#   Input string.
# @param string $2
#   String that will be searched in input string.
# @param int [$3]
#   Offset of an input string.
function strpos {
    local str=${1}
    local offset=${3}

    if [ -n "${offset}" ]; then
        str=`substr "${str}" ${offset}`
    else
        offset=0
    fi

    str=${str/${2}*/}

    if [ "${#str}" -eq "${#1}" ]; then
        return 0
    fi

    echo $((${#str}+${offset}))
}

if [ "$1" = "-s" ]
then

  allDisksInfo=""
  while IFS= read -r line; do
    
    diskData=\"$(echo $line | sed 's/ /,"/g' | sed 's/=/"=/g')
    allDisksInfo+={$diskData

    toCut=$(strpos "${line}" "E=\"")
    toCut=$(($toCut + 3))
    
    diskName=$(substr "${line}" $toCut 4)
    
    label=$(lsblk -n -i -o label /dev/"$diskName") 
    allDisksInfo+=$(echo ,\"LABEL\"=\"$label\" },)
    # },

  done < <(lsblk -n -i -P -o kname,type,mountpoint,fstype,size,uuid | grep sd | grep part)

  echo {"\"DisksInfo\"":[${allDisksInfo: : -1}],

  allDisksUse=""
  while IFS= read -r line; do
    diskData=""

    diskData+=\"$(echo $line | awk '{print $1}')\":{
    diskData+=\"size\"=\"$(echo $line | awk '{print $2}')\",
    diskData+=\"used\"=\"$(echo $line | awk '{print $3}')\",
    diskData+=\"available\"=\"$(echo $line | awk '{print $4}')\",
    diskData+=\"usePercent\"=\"$(echo $line | awk '{print $5}')\"}
    
    allDisksUse+=$diskData,

  done < <(sudo df -h | grep /dev/sd)
  diskUseSize=${#allDisksUse} 
  
  if [ "$diskUseSize" -eq "0" ]
  then
    echo "\"DisksUse\"": null }        
  else
    echo "\"DisksUse\"":{ ${allDisksUse: : -1} }}    
  fi

  # if [ "$?" -eq "0" ]
  # then
  #   exit 0
  # else
  # 	exit 1
  # fi

elif [ "$1" = "-m" ]
then
  
  mount $2 $3

elif [ "$1" = "-mf" ]
then
  mkdir $3

  if [ "$?" -eq "0" ]
  then

    chmod 777 $3

    if [ "$?" -eq "0" ]
    then
      mount $2 $3
      
    else
      exit 1
    fi

  else
    exit 1
  fi


elif  [ "$1" = "-u" ]
then

  umount $2

elif  [ "$1" = "-uf" ]
then

  umount $2

  if [ "$?" -eq "0" ]
  then

    rm_cmd="rm -r"
    cmdToRun="${rm_cmd} '${3}'"

    eval "${cmdToRun}"

    if [ "$?" -eq "0" ]
    then
      exit 0
      
    else
      exit 1
    fi

  else
    exit 1
  fi


else
  echo "ERROR"
  exit 1
fi