<li class="template_procListItem" data-process-id='{{processId}}' data-preventClosure='true'>
  <a class='a_hover_prevent' href='#' data-preventClosure='true'>
    <div>
        <i class='fa fa-comment fa-fw'></i>
        <strong data-preventClosure='true' data-localId='{{localId}}'>{{formattedName}}</strong> 
        <!--<span class='pull-right text-muted small'>4 minutes ago</span>-->
    </div>

    <div class='process_btn_container' >
      <div class='col-xs-3' data-preventClosure='true'>
        <button data-preventClosure='true' data-action='start' data-id='{{processId}}' data-localId='{{localId}}' type='button' class='btn btn-default pM-actionButton {{buttonsStatus.start}}'>
          <i class='fa fa-play'></i>
          </button>
      </div>
      <div class='col-xs-3' data-preventClosure='true'>
        <button data-preventClosure='true' data-action='stop' data-id='{{processId}}' data-localId='{{localId}}' type='button' class='btn btn-default pM-actionButton {{buttonsStatus.stop}}'>
          <i class='fa fa-stop'></i>
        </button>
      </div>
      <div class='col-xs-3' data-preventClosure='true'>
        <button data-preventClosure='true' data-action='restart' data-id='{{processId}}' data-localId='{{localId}}' type='button' class='btn btn-default pM-actionButton {{buttonsStatus.restart}}'>
          <i class='fa fa-refresh'></i>
        </button>
      </div>
      
      <div class='col-xs-3' data-preventClosure='true'>
        <button data-preventClosure='true' data-action='status' type='button' data-id='{{processId}}' data-localId='{{localId}}' class='btn pM-statusButton btn-circle {{statusClass}}' style=' margin-top: 2px;'><i class='fa'></i></button>
      </div>
    </div>
    <div data-preventClosure='true' class="errorField processErrorField" data-localId='{{localId}}'>
      <strong data-preventClosure='true' data-localId='{{localId}}' class="errorField processErrorField" ></strong>
    </div>
  </a>
</li>


