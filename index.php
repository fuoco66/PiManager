<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pi Manager</title>

      <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-theme.css" rel="Stylesheet">

  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    
    <link href="css/theme.css" rel="stylesheet">
    <!--<link href="../css/tablesStyle.css" rel="stylesheet">-->
    <!--<link href="../css/jquery.tablesorter.pager.css" rel="stylesheet">-->
    <!--<link rel="stylesheet" href="../css/contextMenu/jquery.contextMenu.css" type="text/css" media="screen">-->

    <!-- <link href="css/jquery-ui.min.css?v1.1.5" rel="Stylesheet"> -->
    <!-- <link href="css/jquery-ui.theme.min.css?v1.1.5" rel="Stylesheet"> -->

    <!-- Admin 2 CSS-->
    <link href="css/sb-admin-2.css" rel="stylesheet">

    <!-- MetisMenu CSS-->
    <link href="css/metisMenu.css" rel="stylesheet">

    <!-- jQuery -->
    <script type="text/javascript" src="js/jquery-3.2.1.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.js"></script>

    <!--<script type="text/javascript" src="../js/jquery.tablesorter.js"></script>-->
    <!--<script type="text/javascript" src="../js/jquery.tablesorter.pager.js"></script>-->
    <!--<script type="text/javascript" src="../js/jquery.tablesorter.widgets.js"></script>-->

    <!--<script type="text/javascript" src="../js/jquery.ui.position.js"></script>-->
    <!--<script type="text/javascript" src="../js/jquery.contextMenu.js"></script>-->

    <!-- Admin 2 JS-->
    <script src="js/sb-admin-2.js"></script>

    <!-- MetisMenu JS-->
    <script src="js/metisMenu.js"></script>

    <!--Page JS-->
    <script type="text/javascript" src="js/main.js"></script>
    <script type="text/javascript" src="js/util.js"></script>

    <script>
      var serverBaseUrl = "<?php echo $_SERVER['SERVER_ADDR']; ?>";
      var serverRootPath = "<?php define('ROOT_PATH', '/' ); echo ROOT_PATH; ?>";
      InitSystem();
    </script>

  </head>

  <body>
    <!--Includes the Navbar-->
    <?php include('templates/navbar.php'); ?>

    <div id="page-wrapper">

      <div class="row placeholders">

        <div class="col-xs-12 col-md-6 placeholder" data-root="false" data-location="fileManager" data-location-type="absolute">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>File Manager</h4>
        </div>

        <div class="col-xs-12 col-md-6 placeholder" data-root="false" data-location="diskManager" data-location-type="absolute">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Disk Manager</h4>
        </div>

        <div class="col-xs-12 col-md-6 placeholder" data-root="true" data-location="transmission" data-location-type="absolute">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Transmission</h4>
        </div>
        <div class="col-xs-12 col-md-6 placeholder" data-root="false" data-location="coe" data-location-type="absolute">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Coe</h4>
        </div>
        <div class="col-xs-12 col-md-6 placeholder" data-root="false" data-location="lcd" data-location-type="absolute">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Lcd</h4>
        </div>
        <div class="col-xs-12 col-md-6 placeholder" data-root="false" data-location="rom" data-location-type="absolute">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Rom</h4>
        </div>

      </div>

    </div> <!-- /container -->


  </body>
</html>
