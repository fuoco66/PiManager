<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Disk Manager</title>


    
    <link rel="icon" type="img/ico" href="../img/favicon.ico">

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../css/bootstrap-theme.css" rel="Stylesheet">

  	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
    
    <link href="../css/theme.css" rel="stylesheet">
    <link href="../css/tablesStyle.css" rel="stylesheet">
    <!--<link href="../css/jquery.tablesorter.pager.css" rel="stylesheet">-->
    <!--<link rel="stylesheet" href="../css/contextMenu/jquery.contextMenu.css" type="text/css" media="screen">-->

    <!-- Admin 2 CSS-->
    <link href="../css/sb-admin-2.css" rel="stylesheet">

    <!-- MetisMenu CSS-->
    <link href="../css/metisMenu.css" rel="stylesheet">

    <!-- jQuery -->
    <script type="text/javascript" src="../js/jquery-3.2.1.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src="../js/bootstrap.js"></script>

    <script type="text/javascript" src="../js/jquery.tablesorter.js"></script>
    <script type="text/javascript" src="../js/jquery.tablesorter.pager.js"></script>
    <script type="text/javascript" src="../js/jquery.tablesorter.widgets.js"></script>
    <script type="text/javascript" src="../js/jquery.tabledit.js"></script>

    <!--<script type="text/javascript" src="../js/jquery.ui.position.js"></script>-->
    <!--<script type="text/javascript" src="../js/jquery.contextMenu.js"></script>-->

    <script src="../js/Chart.js"></script>
    
    <!-- Admin 2 JS-->
    <script src="../js/sb-admin-2.js"></script>

    <!-- MetisMenu JS-->
    <script src="../js/metisMenu.js"></script>

    <!--Page JS-->
    <script type="text/javascript" src="../js/diskManager.js"></script>
    <script type="text/javascript" src="../js/util.js"></script>

    <script type="text/javascript">
      jQuery.browser = {};
      (function () {
          jQuery.browser.msie = false;
          jQuery.browser.version = 0;
          if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
              jQuery.browser.msie = true;
              jQuery.browser.version = RegExp.$1;
          }
      })();
    </script>

    <script>
      var serverBaseUrl = "<?php echo $_SERVER['SERVER_ADDR']; ?>";
      var serverRootPath = "<?php define('ROOT_PATH', '/' ); echo ROOT_PATH; ?>";
      InitSystem();
    </script>
    
  </head>

  <body>
    <div id="loading" class="loaderDiv">
		  <img src="../img/loading.gif" alt="Loading..." class="loaderImg"/>
	  </div>
    <!--Includes the Navbar-->
    <?php include('../templates/navbar.php'); ?>

    <div id="page-wrapper">
      <div class="row">
          <div class="col-lg-12">
            <h1 class="page-header">
              <div class="" style="display: inline-block;"><span>Disk Manager</span></div>
              
              <div id="toolbarButton_Refresh" style="display: inline-block;">
                <button class="btn btn-default btn-circle btn-lg">
                  <i class="fa fa-refresh"></i>
                </button>
              </div>
              <div id="toolbarButton_Settings" style="display: inline-block;" data-toggle="modal" data-target="#settingsModal">
                <button class="btn btn-default btn-circle btn-lg">
                  <i class="fa fa-cogs"></i>
                </button>
              </div>
              
            </h1>
          </div>
      </div>

      <div class="row">
        <div id="disksContainer" role="main" class="col-lg-12">
        </div>
      </div>

    </div>

    <div class="modal fade" id="settingsModal" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4><span><i class="fa fa-cogs"></i></span> Settings</h4>
          </div>
          <div class="modal-body">
            <div id="knownDisks_loading" class="loaderDiv">
              <img src="../img/loading.gif" class="knownDisks_loaderImg loaderImg" alt="Loading..." />
            </div>
            <div class="knownDisks_Container">
              <div class="knownDisksTable_NoData no_result"><p>No Data</p></div>
              
              <div class="table-responsive">
                <table id="knownDisksTable" class="table table-hover">
                  <thead class="header">
                    <tr>
                      <th>
                        <span>Id</span>
                        <i class="fa fa-sort"></i>
                        <i class="fa fa-sort-asc"></i>
                        <i class="fa fa-sort-desc"></i>
                      </th>
                      <th>
                        <span>Uuid</span>
                        <i class="fa fa-sort"></i>
                        <i class="fa fa-sort-asc"></i>
                        <i class="fa fa-sort-desc"></i>
                      </th>
                      <th>
                        <span>MountPoint</span>
                        <i class="fa fa-sort"></i>
                        <i class="fa fa-sort-asc"></i>
                        <i class="fa fa-sort-desc"></i>
                      </th>
                      <th>
                        <span>PF</span>
                      </th>
                      
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>

              </div>

              <div id="knownDisksTable_pager" class="tablePager">
                <div class="col-md-6 col-xs-12">
                  <div class="col-md-1 col-xs-2">
                    <span class="fa-stack fa-lg first">
                      <i class="fa fa-fast-backward fa-stack-1x"></i>
                    </span>
                  </div>
                  <div class="col-md-1 col-xs-2">
                    <span class="fa-stack fa-lg prev">
                      <i class="fa fa-step-backward fa-stack-1x"></i>
                    </span>
                  </div>
                  <div class="col-md-8 col-xs-4">
                    <span class="pagedisplay"></span>
                  </div>
                  <div class="col-md-1 col-xs-2">
                    <span class="fa-stack fa-lg next">
                      <i class="fa fa-step-forward fa-stack-1x"></i>
                    </span>
                  </div>
                  <div class="col-md-1 col-xs-2">
                    <span class="fa-stack fa-lg last">
                      <i class="fa fa-fast-forward fa-stack-1x"></i>
                    </span>
                  </div>
                </div>

                <div class="col-md-6 col-xs-8">
                  <div class="tablePager-pageSize">
                    <label>Size</label>
                    <input type="text" maxlength="2" class="pagesize pageSizeInput" value="20"></input>
                  </div>
                </div>

              </div>
           </div>
            
          </div>
          <div class="modal-footer">
            <div class="col-md-3 col-sm-2"></div>
            <div class="col-md-6 col-sm-8">
              <button class="btn btn-danger btn-block" data-dismiss="modal">Close</button>
            </div>
            <div class="col-md-3 col-sm-2"></div>      
          </div>
        </div>
      </div>
    </div> 


  </body>
</html>
