<?php
  header('Content-Type: text/event-stream');
  header('Cache-Control: no-cache');

  require_once("../API/ORM_FilesManager.php");
  $FilesManager = new ORM_FilesManager();
  $errorCode = null;
  $errorMessage = null;
  $res = $FilesManager->GetCopyStatus(false, $errorCode, $errorMessage);

  echo 'data: ' . json_encode($res) . PHP_EOL;
  // echo 'data2: ' . json_encode($res) . PHP_EOL;


  echo PHP_EOL;
?>
