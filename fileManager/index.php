<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>File Manager</title>


    
    <link rel="icon" type="img/ico" href="../img/favicon.ico">

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../css/bootstrap-theme.css" rel="Stylesheet">

  	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
    
    <link href="../css/theme.css" rel="stylesheet">
    <link href="../css/tablesStyle.css" rel="stylesheet">
    <link href="../css/jquery.tablesorter.pager.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/contextMenu/jquery.contextMenu.css" type="text/css" media="screen">

    <!-- Admin 2 CSS-->
    <link href="../css/sb-admin-2.css" rel="stylesheet">

    <!-- MetisMenu CSS-->
    <link href="../css/metisMenu.css" rel="stylesheet">

    <!-- jQuery -->
    <script type="text/javascript" src="../js/jquery-3.2.1.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src="../js/bootstrap.js"></script>

    <script type="text/javascript" src="../js/jquery.tablesorter.js"></script>
    <script type="text/javascript" src="../js/jquery.tablesorter.pager.js"></script>
    <script type="text/javascript" src="../js/jquery.tablesorter.widgets.js"></script>

    <script type="text/javascript" src="../js/jquery.ui.position.js"></script>
    <script type="text/javascript" src="../js/jquery.contextMenu.js"></script>

    <!-- Admin 2 JS-->
    <script src="../js/sb-admin-2.js"></script>

    <!-- MetisMenu JS-->
    <script src="../js/metisMenu.js"></script>

    <!--Page JS-->
    <script type="text/javascript" src="../js/fileManager.js"></script>
    <script type="text/javascript" src="../js/util.js"></script>

    <script type="text/javascript">
      jQuery.browser = {};
      (function () {
          jQuery.browser.msie = false;
          jQuery.browser.version = 0;
          if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
              jQuery.browser.msie = true;
              jQuery.browser.version = RegExp.$1;
          }
      })();
    </script>

    <script>
      var serverBaseUrl = "<?php echo $_SERVER['SERVER_ADDR']; ?>";
      var serverRootPath = "<?php define('ROOT_PATH', '/' ); echo ROOT_PATH; ?>";
      InitSystem();
    </script>
    
  </head>

  <body>
    <!--Includes the Navbar-->
    <?php include('../templates/navbar.php'); ?>

    <div id="page-wrapper">
      <div class="row">
          <div class="col-lg-12">
              <h1 class="page-header">Dashboard</h1>
          </div>
      </div>

      <div class="row">
        <div role="main" class="col-lg-12">
          <div class="row">
            <div class="col-md-11">
              <h3><span id="currentFolderName"></span></h3>
            </div>
            <div id="directoryOpLoader" class="col-md-1 load_icon">
              <h3> <i class="fa fa-spinner fa-pulse fa-fw"> </i>
            </div>
          </div>

          <!-- Start Toolbar -->
          <div class="row col-md-12">
            <div id="toolbarButton_SelectorToggle" data-function="selectorToggle" class="col-md-1 col-xs-2 hidden-md hidden-lg hidden-sm toolbarButton notActive">
              <button class="btn btn-default btn-circle btn-lg col-md-1 col-xs-2">
                <i class="fa fa-check-square"></i>
              </button>
            </div>

            <div id="toolbarButton_Back" data-function="back" class="col-md-1 col-xs-2 toolbarButton disabled">
              <button class="btn btn-default btn-circle btn-lg col-md-1 col-xs-2">
                <i class="fa fa-arrow-left"></i>
              </button>
            </div>

            <div id="toolbarButton_Forward" data-function="forward" class="col-md-1 col-xs-2 toolbarButton disabled">
              <button class="btn btn-default btn-circle btn-lg col-md-1 col-xs-2">
                <i class="fa fa-arrow-right"></i>
              </button>
            </div>

            <div class="col-md-6 col-xs-1 hidden-xs disabled"></div>

            <div id="toolbarButton_Paste" data-function="paste" class="col-md-1 col-xs-2 toolbarButton disabled">
              <button class="btn btn-default btn-circle btn-lg col-md-1 col-xs-2">
                <i class="fa fa-clipboard"></i>
              </button>
            </div>

            <div id="toolbarButton_Delete" data-function="delete" class="col-md-1 col-xs-2 toolbarButton disabled">
              <button class="btn btn-default btn-circle btn-lg col-md-1 col-xs-2">
                <i class="fa fa-trash-o"></i>
              </button>
            </div>

            <div id="toolbarButton_Refresh" data-function="refresh" class="col-md-1 col-xs-2 toolbarButton">
              <button class="btn btn-default btn-circle btn-lg col-md-1 col-xs-2">
                <i class="fa fa-refresh"></i>
              </button>
            </div>

          </div>

          <div class="row col-md-12">
            <div class="row col-md-12 fileTable_NoData no_result"><p>No Data</p></div>
            <div class="row col-md-12 table-responsive">

              <table id="fileTable" class="table table-hover">
                <thead class="header">
                  <tr>
                    <th>
                      <span>File Name</span>
                      <i class="fa fa-sort"></i>
                      <i class="fa fa-sort-asc"></i>
                      <i class="fa fa-sort-desc"></i>
                    </th>
                    <th>
                      <span>Type</span>
                      <i class="fa fa-sort"></i>
                      <i class="fa fa-sort-asc"></i>
                      <i class="fa fa-sort-desc"></i>
                    </th>
                    <th>
                      <span>Size</span>
                      <i class="fa fa-sort"></i>
                      <i class="fa fa-sort-asc"></i>
                      <i class="fa fa-sort-desc"></i>
                    </th>
                    <th>
                      <span>Date</span>
                      <i class="fa fa-sort"></i>
                      <i class="fa fa-sort-asc"></i>
                      <i class="fa fa-sort-desc"></i>
                    </th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>

            </div>

            <div id="fileTable_pager" class="row col-md-12 tablePager">
              <div class="col-md-6 col-xs-12">
                <div class="col-md-1 col-xs-2">
                  <span class="fa-stack fa-lg first">
                    <i class="fa fa-fast-backward fa-stack-1x"></i>
                  </span>
                </div>
                <div class="col-md-1 col-xs-2">
                  <span class="fa-stack fa-lg prev">
                    <i class="fa fa-step-backward fa-stack-1x"></i>
                  </span>
                </div>
                <div class="col-md-8 col-xs-4">
                  <span class="pagedisplay"></span>
                </div>
                <div class="col-md-1 col-xs-2">
                  <span class="fa-stack fa-lg next">
                    <i class="fa fa-step-forward fa-stack-1x"></i>
                  </span>
                </div>
                <div class="col-md-1 col-xs-2">
                  <span class="fa-stack fa-lg last">
                    <i class="fa fa-fast-forward fa-stack-1x"></i>
                  </span>
                </div>
              </div>

              <div class="col-md-6 col-xs-8">
                <div class="tablePager-pageSize">
                  <label>Size</label>
                  <input type="text" maxlength="2" class="pagesize pageSizeInput" value="20"></input>
                </div>
              </div>
            </div>

          </div>
        </div>

      </div>


    </div>

  </body>
</html>
