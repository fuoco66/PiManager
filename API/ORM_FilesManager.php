<?php

  class ORM_FilesManager {

    // Costruttore
    public function __construct(){ }

    public function GetDirectoryObjects($currentDirectory, &$errorCode, &$errorMessage) {
      $filesArray = array();
      $canGoBack = false;
      $directoryObj = opendir($currentDirectory);

      $rootRawTime = filemtime($currentDirectory);
      $rootDateObj = new DateTime();
      $rootFormattedTime = $rootDateObj->setTimestamp($rootRawTime);
      $rootFormattedTime = $rootDateObj->format('d/m/Y, H:i:s');
      $rootPermission = fileperms($currentDirectory);

      while($entryName=readdir($directoryObj)) {
        $dirArray[] = $entryName;
      }

      closedir($directoryObj);

      // Counts elements in array
      $indexCount = count($dirArray);
      sort($dirArray);


      for($index=0; $index < $indexCount; $index++) {

        $fileCompletePath = $currentDirectory . '/' . $dirArray[$index];
        // Gets File Names
        $name = $dirArray[$index];
        // Gets Extensions
        $objectClass = null;
        $extn = $this->Findexts($currentDirectory, $dirArray[$index]);
        $type = $this->FindFileType($extn, $objectClass);

        $formattedSize = "";
        $rawSize = "";

        // Gets Date Modified Data
        $rawTime = filemtime($fileCompletePath);
        $dateObj = new DateTime();

        $formattedTime = $dateObj->setTimestamp($rawTime);
        $formattedTime = $dateObj->format('d/m/Y, H:i:s');

        if($extn != "dir"){
          // Gets file size
          $rawSize = filesize($fileCompletePath);
          $formattedSize = $this->FormatSizeUnits($rawSize);

        } elseif ($name == ".") {
          continue;
        } elseif ($name == "..") {
          continue;
          // $name = ".. (Parent Directory)";
          // $extn = "parentDir";
          // $objectClass = "parentDir";

        } else {
          #$extn ="&lt;Dir&gt;";
        }

        $file = array(
                  "id" => null,
                  "name" => $name,
                  "completePath" => $fileCompletePath,
                  "objClass" => $objectClass,
                  "extn" => $extn,
                  "type" => $type,
                  "sizeRaw" => $rawSize,
                  "sizeFormatted" => $formattedSize,
                  "timeRaw" => $rawTime,
                  "timeFormatted" => $formattedTime
                );
        array_push($filesArray, $file);

      }

      return array(
                "rootPermission" => $rootPermission,
                "rootRawTime" => $rootRawTime,
                "rootFormattedTime" => $rootFormattedTime,
                "objectsCount" => intval($indexCount),
                "canGoBack" => $currentDirectory == "/media" ? false : true,
                "filesList" => $filesArray
              );

    }

    public function DeleteFiles($filesArray, &$errorCode, &$errorMessage){

      if( !isset($filesArray)){
        $errorCode = -9999;
        $errorMessage = 'EMPTY_PARAM';
        return null;
      }

      $deletedFiles = array();
      $notDeteletedFiles = array();

      foreach ($filesArray as $value) {

        $output = $this->shexec("../fileManager/script/deleteFile " . escapeshellarg($value->completePath)  );

        // check if error in execution
        if($output["stderr"] != "") {
          $errorMessage .= $output["stderr"] . "\n";
          array_push($notDeteletedFiles, $value->dataLocalId);
        } else {
          array_push($deletedFiles, $value->dataLocalId);
        }
      }

      return array(
                "filesDeleted" => $deletedFiles,
                "filesNotDeleted" => $notDeteletedFiles,
                "rawError" => $errorMessage == "" ? null : $errorMessage
              );
    }

    public function CopyObject($source, $destination, &$errorCode, &$errorMessage) {
      if( !isset($source) || !file_exists($source->path) || !$source->name != '' ){
        $errorCode = -9999;
        $errorMessage = 'SOURCE_NOT_EXIST';
        return null;
      }

      //da gestire il file duplicato
      if( !isset($destination) || !file_exists($destination->path) ){
        $errorCode = -9999;
        $errorMessage = 'DESTINATION_NOT_EXIST';
        return null;
      }

      if( !is_dir($destination->path) ){
        $errorCode = -9999;
        $errorMessage = 'DESTINATION_NOT_VALID';
        return null;
      }

      $copyLogPath = "../fileManager/copyLog.log";

      $sourceName = $source->name;
      $sourcePath = $source->path;
      $destinationPath = $destination->path;

      $duplicateIndex = 1;
      if(file_exists($destinationPath . $sourceName)){
        $ext = explode(".", $sourceName);
        $ext = "." . $ext[count($ext)-1];
        $sourceNameNoExt = substr($sourceName, 0, strpos($sourceName, $ext));

        $newSourceName = $sourceNameNoExt . "_" . $duplicateIndex . "_" . $ext;

        while(file_exists($destinationPath . $newSourceName)){
          $newSourceName = $sourceNameNoExt . "_" . $duplicateIndex . "_" . $ext;
          $duplicateIndex++;
        }

        $destinationPath = $destinationPath;
        $sourceName = $newSourceName;
      }
      $str = $this->exec_bg("../fileManager/script/copyFile " . escapeshellarg($sourceName) . " " . escapeshellarg($sourcePath) . " " . escapeshellarg($destinationPath), $copyLogPath);

      return $str;
    }

    private function exec_bg($cmd, $copyLogPath) {
      return shell_exec($cmd . " > " . $copyLogPath . " & echo $!");
    }

    public function GetCopyStatus($noProgress = false, &$errorCode, &$errorMessage){
      $isCopyRunning = false;
      $data = null;
      $status = null;
      $CopyInfo = null;

      $copyProgressData = null;

      $filename = "../fileManager/copyLog.log";

      //check if a copy is running
      $output = $this->shexec("../fileManager/script/copyStatus.sh -s {$filename}");

      //check if error in execution
      if($output["exit"] != 0) {
        $errorCode = $output["exit"];
        $errorMessage = $output["stderr"];
        return;
      }


      if(strrpos($output["stdout"], "@@") != false){

        $isCopyRunning = false;

        $data = explode("@@", str_replace("\n", "",$output["stdout"]) );
        $status = $data[0] == "OK" ? "IDLE_OK" : "IDLE_ERROR";

      } else {

        $output = $this->shexec("../fileManager/script/copyStatus.sh -sv {$filename}");
        if($output["exit"] != 0) {
          $errorCode = $output["exit"];
          $errorMessage = $output["stderr"];
          return;
        }
        $data = explode("@@", str_replace("\n", "",$output["stdout"]) );
        $isCopyRunning = true;
        $status = "RUNNING";
      }


      $CopyInfo = array (
        "isCopyRunning" => $isCopyRunning,
        "status" => $status,
        "objectData" => array(
          "sourceFileName" => $data[1],
          "sourceFileSize" => file_exists($data[2]) ? $this->FormatSizeUnits(fileSize($data[2])) : "0 bytes",
          "sourceFullPath" => $data[2],
          "destinationPath" => $data[3],
          "destinationFullPath" => $data[4]
        )

      );

      if(!$noProgress && $isCopyRunning){

        // //check the status of the running copy
        $output = $this->shexec("../fileManager/script/copyStatus.sh -p {$filename}");

        //check if error in execution
        if($output["stderr"] != "") {
          $errorCode = 1;
          $errorMessage = $output["stderr"];
          return;
        }

        $resultArray = explode("\n", $output["stdout"]);

        $copyProgressData = array(
              "sentData" =>  $resultArray[0],
              "speed" => $resultArray[2],
              "percentage" => $resultArray[1],
              "eta" => $resultArray[3],
                 );

        $CopyInfo["progress"] = $copyProgressData;

      }

      return $CopyInfo;
    }

    public function StopAllCopyTask(&$errorCode, &$errorMessage){
      $filename = "../fileManager/copyLog.log";
      $output = $this->shexec("../fileManager/script/stopCopy");

      //check if error in execution
      if($output["exit"] != 0) {
        $errorCode = $output["exit"];
        $errorMessage = $output["stderr"];
        return;
      }

      if($output["stderr"] != "") {
        $errorCode = 1;
        $errorMessage = $output["stderr"];
        return;
      }
      return "OK";

    }

    /**
   * Executes the shell command and gets stdout and stderr streams
   *
   * @see http://www.php.net/manual/en/function.proc-open.php
   * @param string $cmd - command to be executed
   * @param string $cwd - folder
   * @param array $env - options
   * @return array()
   */
    private function shexec($cmd, $cwd = './', $env = array()){
        $return_value = array(
             "exit"   => 1,       // exit 0 on ok
             "stdout" => "",      // output of the command
             "stderr" => "",      // errors during execution
        );

        $descriptorspec = array(
        0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
        1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
        2 => array("pipe", "w")   // stderr is a pipe
        );

        // $process = proc_open(escapeshellcmd($cmd), $descriptorspec, $pipes, $cwd, $env);         
        $process = proc_open($cmd, $descriptorspec, $pipes, $cwd, $env);
        // $pipes now looks like this:
        // 0 => writeable handle connected to child stdin
        // 1 => readable handle connected to child stdout
        // 2 => readable handle connected to child stderr

        if (false === is_resource($process))
        {
            //echo("Sys::shexec() Error on proc_open, shell command \"$cmd\"");
        }
        else
        {
            $return_value['stdout'] = stream_get_contents($pipes[1]);
            $return_value['stderr'] = stream_get_contents($pipes[2]);

            fclose($pipes[0]);
            fclose($pipes[1]);
            fclose($pipes[2]);

            // It is important that you close any pipes before calling
            // proc_close in order to avoid a deadlock
            $return_value['exit'] = proc_close($process);
        }

        if(trim($return_value['stderr']) !== "") {
            //echo("Sys::shexec() \n\"$cmd\"\nERROR:\n" . $return_value['stderr']);
        }

        if(trim($return_value['stdout']) !== "") {
            //echo("Sys::shexec() \n\"$cmd\"\nOUTPUT:\n" . $return_value['stdout']);
        }

        return $return_value;

    } // END FUNCTION shexec()

    private function FindFileType(&$extension, &$objectClass){

      switch ($extension){
        case "png": $objectClass = "file"; return "PNG Image"; break;
        case "jpg": $objectClass = "file"; return "JPEG Image"; break;
        case "svg": $objectClass = "file"; return "SVG Image"; break;
        case "gif": $objectClass = "file"; return "GIF Image"; break;
        case "ico": $objectClass = "file"; return "Windows Icon"; break;

        case "mkv": $objectClass = "file"; return "Video"; break;
        case "mp4": $objectClass = "file"; return "Video"; break;
        case "avi": $objectClass = "file"; return "Video"; break;
        case "3pg": $objectClass = "file"; return "Video"; break;

        case "txt": $objectClass = "file"; return "Text File"; break;
        case "log": $objectClass = "file"; return "Log File"; break;
        case "htm": $objectClass = "file"; return "HTML File"; break;
        case "php": $objectClass = "file"; return "PHP Script"; break;
        case "js" : $objectClass = "file"; return "Javascript"; break;
        case "css": $objectClass = "file"; return "Stylesheet"; break;
        case "pdf": $objectClass = "file"; return "PDF Document"; break;

        case "zip": $objectClass = "file"; return "ZIP Archive"; break;
        case "bak": $objectClass = "file"; return "Backup File"; break;

        case "bak": $objectClass = "file"; return "Backup File"; break;
        case "Dir":
          $objectClass = "dir";
          $extension = "dir";
          return "Dir";
        break;

        default: $objectClass = "file"; return strtoupper($extension)." File"; break;
      }

    }

    private function Findexts($path ,$fileName){
      if(is_dir($path . '/' . $fileName) !== True)
      {
        $fileName = strtolower($fileName);
        // $exts=split("[/\\.]", $fileName);
        $exts = explode(".", $fileName);
        $n=count($exts)-1;
        $exts=$exts[$n];
      }
      else
      {
        $exts = "Dir";
      }
      return $exts;
    }

    private function FormatSizeUnits($bytes){

      if ($bytes >= 1073741824)
      {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
      }
      elseif ($bytes >= 1048576)
      {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
      }
      elseif ($bytes >= 1024)
      {
        $bytes = number_format($bytes / 1024, 2) . ' KB';
      }
      elseif ($bytes > 1)
      {
        $bytes = $bytes . ' bytes';
      }
      elseif ($bytes == 1)
      {
        $bytes = $bytes . ' byte';
      }
      else
      {
        $bytes = '0 bytes';
      }

      return $bytes;
    }


  }

?>
