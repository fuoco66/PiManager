$( document ).ready(function() {

  $('.placeholder').click(function(){
    ActionSelected($(this));
  });

});

function ActionSelected(thisField){
  var location = thisField.attr('data-location');
  var dataLocationType = thisField.attr('data-location-type');
  var dataRoot = thisField.attr('data-root');

  var destinationUrl = "";

  if(dataRoot == "true"){
    destinationUrl = serverBaseUrl + "/" + location;
  } else {
    destinationUrl = serverBaseUrl + serverRootPath + "/" + location;
  }

  if (dataLocationType == "file"){
    destinationUrl+= ".php";
  }

  $(window.location).attr('href', 'http://' + destinationUrl);

}
