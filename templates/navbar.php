<script type="text/javascript" src="../js/navbar.js"></script>
<script type="text/javascript" src="../js/processManager.js"></script>
<script type="text/javascript" src="../js/mustache.js" ></script>

<script>
  var preventMenuCloseIds = [ 'abortCopy', 'copyInfoContainer', 'processManager' ];
</script>

<div id="procTemplate"  style="display: none;"></div>

<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="index.html">SB Admin v2.0</a>
  </div>
    <!-- /.navbar-header -->

  <ul class="nav navbar-top-links navbar-right">

    <li id="processManager" class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
          <i class="fa fa-terminal fa-fw"></i> <i class="fa fa-caret-down"></i>
        </a>
        
        <ul id="pM_processList" class="dropdown-menu dropdown-messages">
          <li data-preventClosure='true'>
            <div class="text-center a_hover_prevent" href="#">
              
              <span id="processManager_loader" class='load_icon'>
                <i class='fa fa-spinner fa-pulse fa-fw'></i>
              </span>
              <strong>Processes</strong>
              
              <button id="pM_btn_settings" data-preventClosure='true' class="btn btn-default btn-circle btn-sm">
                <i class="fa fa-cogs"></i>
              </button>              

            </div>
          </li>
          <li class="divider" data-preventClosure='true'></li>
          
        </ul>
    </li>
    
    <li id="copyMonitor" class="dropdown">
      <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="fa fa-tasks fa-fw"></i> <i class="fa fa-caret-down"></i>
      </a>
      <ul class="dropdown-menu dropdown-tasks">
        <li>

          <?php include('copyMonitor.php'); ?>

        </li>
      </ul>
    </li>

  </ul>

    <div class="navbar-default sidebar" role="navigation">
      <div class="sidebar-nav navbar-collapse collapse" aria-expanded="false">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
              <div class="input-group custom-search-form">
                <input type="text" class="form-control" placeholder="Search..." />
                  <span class="input-group-btn">
                  <button class="btn btn-default" type="button">
                    <i class="fa fa-search"></i>
                  </button>
                </span>
              </div>
            </li> 
            <li>
                <a href="<?php echo ROOT_PATH; ?>"><i class="fa fa-dashboard fa-fw"></i> Home</a>
            </li>
            <li>
              <a href="#"><i class="fa fa-folder-open-o fa-fw"></i> FileManager<span class="fa arrow"></span></a>
              <ul class="nav nav-second-level">
                <li>
                  <a class="advancedLink" data-deepLink="fileManager@/media" href="#/media"> Home</a>
                </li>
                <li>
                  <a class="advancedLink" data-deepLink="fileManager@/media/Data/Downloads" href="#/media/Data/Downloads"> Downloads</a>
                </li>
              </ul>
                <!-- /.nav-second-level -->
            </li>

            <li>
              <a href="<?php echo ROOT_PATH; ?>diskManager"><i class="fa fa-hdd-o fa-fw"></i> DiskManager</a>
            </li>

            <li>
              <a href="<?php echo ROOT_PATH; ?>transmission"><i class="fa fa-table fa-fw"></i> Transmission</a>
            </li>
        </ul>
      </div>
    </div>
    <!-- /.navbar-static-side -->
  </nav>