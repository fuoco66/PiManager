<?php

  require_once("ORM_Util.php");

  class ORM_DiskManager {

    // Costruttore
    public function __construct(){ }

    public function DisksStatus($disk = null, &$errorCode, &$errorMessage){
      $UtilObject = new ORM_Util();
      $diskUse = null;

      $output = $UtilObject->shexec("../diskManager/script/diskManager -s null null");

      // check if error in execution
      if($output["stderr"] != "") {
        $errorCode = -2001;        
        $errorMessage = $output["stderr"] . "\n";
        return null;
      }
      // return str_replace('=', ':', $output['stdout']);
      $diskRawData = json_decode(str_replace('=', ':', $output['stdout']));      
      $disksFormattedData = array();

      if(isset($diskRawData->DisksUse)){
        $diskUse = get_object_vars($diskRawData->DisksUse);
      } 
      // $diskUse2 = $diskRawData->DisksUse;

      $count = 0;
      foreach ($diskRawData->DisksInfo as $Disk) {
        if(isset($disk) && $disk != "/dev/" . $Disk->KNAME){
          continue;
        }

        $diskFormatted = array(
          "id" => $count,
          "name" => "/dev/" . $Disk->KNAME,
          "mountPoint" => $Disk->MOUNTPOINT != '' ? $Disk->MOUNTPOINT : null ,
          "fileSystem" => $Disk->FSTYPE,
          "size" => $Disk->SIZE,
          "label" => $Disk->LABEL != '' ? $Disk->LABEL : null,
          "uuid" => $Disk->UUID,
          "used"=> null,
          "available"=> null,
          "usePercent"=> null
        ); 

        if(isset($diskUse) && array_key_exists("/dev/" . $Disk->KNAME, $diskUse)){
          $currentDiskUse = $diskUse["/dev/" . $Disk->KNAME];

          $diskFormatted['used'] = $currentDiskUse->used;
          $diskFormatted['available'] = $currentDiskUse->available;
          $diskFormatted['usePercent'] = $currentDiskUse->usePercent;
        }
          
        array_push($disksFormattedData, $diskFormatted);
        $count ++;
      }

      return $disksFormattedData;
       
    }


    public function DiskMount($disk, &$errorCode, &$errorMessage){
      
      $UtilObject = new ORM_Util();
      $diskName = $disk->name;
      $diskUuid = $disk->uuid;
      
      $knowkDisks = $this->GetKnownDisks(true, null, $errorCode, $errorMessage);

      $knownMountError = "The disk contains an unclean file system (0, 0).The file system wasn't safely closed on Windows. Fixing.";

      $mountFolder = "/media/" . $diskUuid;
      $cmdOptions = "-m";

      // check if it's a known drive
      if(array_key_exists($diskUuid, $knowkDisks)){
        $mountFolder = $knowkDisks[$diskUuid];
      } 

      if(!file_exists($mountFolder)) {
        $cmdOptions = "-mf";
      }

      $output = $UtilObject->shexec("../diskManager/script/diskManager " . $cmdOptions . " " . escapeshellarg($diskName) . " " . escapeshellarg($mountFolder));

      // check if error in execution
      if($output["stderr"] != "" && $output["stderr"] != $knownMountError) {
        $errorCode = -2001;
        $errorMessage = $output["stderr"];
        return null;
      }

      $diskStatus = $this->DisksStatus(null, $errorCode, $errorMessage);

      return $diskStatus;

    }

    public function DiskUmount($disk, &$errorCode, &$errorMessage){
      $UtilObject = new ORM_Util();
      $diskName = $disk->name;
      $diskUuid = $disk->uuid;
      $diskMountPoint = $disk->mountPoint;
      $cmdOptions = "-uf";
      
      $knowkDisk = $this->GetKnownDisks(false, $disk->uuid, $errorCode, $errorMessage);
      if( isset($knowkDisk) && $knowkDisk['persistentFolder'] == 1 && $disk->mountPoint == $knowkDisk['mountPoint']){
        //set the command to remove the folder
        $cmdOptions = "-u";
      } 

      $output = $UtilObject->shexec("../diskManager/script/diskManager " . $cmdOptions . " " . escapeshellarg($diskName) . " " . escapeshellarg($diskMountPoint));

      // check if error in execution
      if($output["stderr"] != "") {
        $errorCode = -2002;
        $errorMessage = $output["stderr"];
        return null;

        //consider adding lsof | grep [mountPoint]
        //to get the process accessing the device
      }

      $diskStatus = $this->DisksStatus(null, $errorCode, $errorMessage);

      return $diskStatus;      
    }

    public function GetKnownDisks($associative, $uuidFilter, &$errorCode, &$errorMessage){
  	  $UtilObject = new ORM_Util();
      $dbConn = $UtilObject->GetDbConnection($errorCode, $errorMessage);
      
      if(!isset($dbConn)){
        $errorCode = -2003;
				$errorMessage = "NO_CONNECTION";
				return null;
      }

      $sqlStr = "SELECT *
            FROM knownDisks";

      if(isset($uuidFilter)){
        $sqlStr .= " WHERE UUID='" . $uuidFilter . "' ";
      }
      
      $sqlStr .= " ORDER BY entryId";

      $queryRes = $dbConn->query($sqlStr);

      $knowkDisks = array();

      if(isset($uuidFilter)){
        return $queryRes->fetch_assoc();
      }

      while ($row = $queryRes->fetch_assoc()) {

        if(!$associative) {
          $newDisk = array(
            "uuid" => $row['uuid'],
            "mountPoint" => $row['mountPoint'],
            "entryId" => $row['entryId'],
            "persistentFolder" => intval($row['persistentFolder']),
          );
          array_push($knowkDisks, $newDisk);

        } else{
          $knowkDisks[$row['uuid']] = $row['mountPoint'];   
        }
            
      }

      return $knowkDisks;

    }

    public function UpdateKnownDisk($disk, &$errorCode, &$errorMessage){

      if(!isset($disk)){
        $errorCode = -2004;
				$errorMessage = "EMPTY_PARAM";
				return null;
      }

      $UtilObject = new ORM_Util();
      $dbConn = $UtilObject->GetDbConnection($errorCode, $errorMessage);
      
      if(!isset($dbConn)){
        $errorCode = -2005;
				$errorMessage = "NO_CONNECTION";
				return null;
      }

      $mountPoint = "'" . $dbConn->real_escape_string($disk->mountPoint) . "'";
      $uuid = "'" . $dbConn->real_escape_string($disk->uuid) . "'";
      $entryId = intval($disk->entryId);

      $sqlStr = "UPDATE knownDisks
                 SET mountPoint=" . $mountPoint . 
                   ", uuid=" . $uuid .
                   ", dateUpdate=NOW()" .
               " WHERE entryId=" . $entryId;

      if ($dbConn->query($sqlStr) !== true) {
        $errorCode = -2006;
				$errorMessage = $dbConn->error;
				return null;

      }

      $dbConn->close();

      return $disk;      
    }

    public function DeleteKnownDisk($disk, &$errorCode, &$errorMessage){

      if(!isset($disk)){
        $errorCode = -2006;
				$errorMessage = "EMPTY_PARAM";
				return null;
      }

      $UtilObject = new ORM_Util();
      $dbConn = $UtilObject->GetDbConnection($errorCode, $errorMessage);
      
      if(!isset($dbConn)){
        $errorCode = -2007;
				$errorMessage = "NO_CONNECTION";
				return null;
      }

      $entryId = intval($disk->entryId);

      $sqlStr = "DELETE FROM knownDisks WHERE entryId=" . $entryId;

      if ($dbConn->query($sqlStr) !== true) {
        $errorCode = -2008;
				$errorMessage = $dbConn->error;
				return null;

      }

      $dbConn->close();


      return $disk;       
    }

    public function AddKnownDisk($disk, &$errorCode, &$errorMessage){

      if(!isset($disk)){
        $errorCode = -2009;
				$errorMessage = "EMPTY_PARAM";
				return null;
      }

      $UtilObject = new ORM_Util();
      $dbConn = $UtilObject->GetDbConnection($errorCode, $errorMessage);
      
      if(!isset($dbConn)){
        $errorCode = -2010;
				$errorMessage = "NO_CONNECTION";
				return null;
      }

      $mountPoint = "'" . $dbConn->real_escape_string($disk->mountPoint) . "'";
      $uuid = "'" . $dbConn->real_escape_string($disk->uuid) . "'";

      $sqlStr = "INSERT INTO knownDisks (uuid, mountPoint, persistentFolder, dateUpdate)
                 VALUES(" . $uuid . ", " . $mountPoint . ", " . intval($disk->persistentFolder) . ", NOW())";

      if ($dbConn->query($sqlStr) !== true) {
        $errorCode = -2011;
				$errorMessage = $dbConn->error;
				return null;

      }

      $dbConn->close();


      return null;      
    }
    

  }

?>
