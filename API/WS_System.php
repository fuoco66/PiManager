<?php
  header('Content-type: application/json');

  require_once("ORM_Util.php");
  require_once("ORM_SystemManager.php");

  session_start();
  clearstatcache();

  $UtilObject = new ORM_Util();
  $SystemManager = new ORM_SystemManager();

  $errorCode = 0;
  $errorMessage = "";
  $return_object = "OK";

  $JSON = $UtilObject->VerifyWsParameters($errorCode, $errorMessage);

  switch ($JSON->action) {
    case "get_process_status":
    
      $return_object = $SystemManager->GetProcessStatus($JSON->value, $errorCode, $errorMessage);
      
    break;
    case "process_action":
      $return_object = $SystemManager->ProcessAction($JSON->value, $errorCode, $errorMessage);
    
    break;
    default:

      $errorCode = -666;
      $errorMessage = "INVALID_REQUEST";

      break;

  }

  echo $UtilObject->ManageWebServiceResponse($return_object, $errorCode, $errorMessage);

?>
