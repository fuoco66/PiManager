var graphOptions = {
  responsive: true,

  maintainAspectRatio: false,
  ///Boolean - Whether grid lines are shown across the chart
  scaleShowGridLines : true,
  //String - Colour of the grid lines
  //scaleGridLineColor : "rgba(0,0,0,.05)",
  //Number - Width of the grid lines
  scaleGridLineWidth : 1,
  //Boolean - Whether to show horizontal lines (except X axis)
  scaleShowHorizontalLines: true,
  //Boolean - Whether to show vertical lines (except Y axis)
  scaleShowVerticalLines: true,
  //Boolean - Whether the line is curved between points
  bezierCurve : true,
  //Number - Tension of the bezier curve between points
  bezierCurveTension : 0.4,
  //Boolean - Whether to show a dot for each point
  pointDot : true,
  //Number - Radius of each point dot in pixels
  pointDotRadius : 4,
  //Number - Pixel width of point dot stroke
  pointDotStrokeWidth : 1,
  //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
  pointHitDetectionRadius : 20,
  //Boolean - Whether to show a stroke for datasets
  datasetStroke : true,
  //Number - Pixel width of dataset stroke
  datasetStrokeWidth : 2,
  //Boolean - Whether to fill the dataset with a colour
  datasetFill : true,
  legend: {
            position: 'bottom',
          },
  animation: {
      animateScale: true,
      animateRotate: true
  } 

};

var CacheDiskData = null;
var KnownDisksUuid = [];

$( document ).ready(function() {
  GetDisksData();

  $('#toolbarButton_Refresh').click(function (e) {
    GetDisksData();
  });

  $("#settingsModal").on('show.bs.modal', function () {
    
    $('#knownDisks_loading').fadeIn('fast');
    LoadKnowndisks();
  });

  $("#settingsModal").on('hidden.bs.modal', function () {
    // console.log('The modal is fully closed.');
  });
    
});

function GetDisksData(){

  var dataJSON = {
    action: "disks_status",
    value: null
  }

  $("#loading").fadeIn(300);

  $.ajax({
    type: "POST",
    url: "https://" + serverBaseUrl + serverRootPath + "/API/WS_Disks.php",
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {
      if( msg.code == 0 ) {
        CacheDiskData = msg.response;
        RenderDiskGraphics(msg.response)

      } else {
        console.log("ERROR");
        $("#loading").fadeOut(300);
        // $("#directoryOpLoader").removeClass('waiting_ajax');
      }
    },
    error: function(msg) {
      console.log("FATAL_ERROR");
      $("#loading").fadeOut(300);
      // $("#directoryOpLoader").removeClass('waiting_ajax');
    },
    complete: function() {
    }
  });

}

function RenderDiskGraphics(diskData){
  var DisksContainer = $("#disksContainer");

  DisksContainer.empty();
  for( i = 0; i < diskData.length; i++ ) {
    var currentDisk = diskData[i];
    GenerateDiskElement(currentDisk, DisksContainer, false);

  }

  DisksContainer = null;
  
  $("#loading").fadeOut(300);
}

function GenerateDiskElement(diskData, disksContainer, update){
  var isDiskMounted = diskData.mountPoint != null ? true : false;

  var diskUsedPercent = isDiskMounted == true ? parseInt(diskData.usePercent.replace("%", "")) : 100;
  var diskFreePercent = isDiskMounted == true ? 100 - diskUsedPercent : 0;
  
  var menuVisibility = isDiskMounted == true ? "style='display: none;'" : '';
  var elementVisibility = isDiskMounted == true ? 'null' : "style='display: none;'";
  var otherButtonClasses = isDiskMounted == true ? '' : 'disabled';

  var diskId = update == false ? diskData.id : $("div.diskDataContainer[data-diskName='" + diskData.name + "']").attr("data-diskId");

  var actionsButtons = isDiskMounted == true ?  "<div class='col-lg-2'>" +
                                                "</div>" +
                                                "<div class='col-lg-4'>" +
                                                  "<button data-action='umount' data-diskId='" + diskId + "' type='button' class='btn btn-block btn-danger diskAction'>Umount</button>" +                                                 
                                                "</div>" +
                                                "<div class='col-lg-4'>" +
                                                  "<button data-action='fileManager' data-diskId='" + diskId + "' type='button' class='btn btn-block btn-primary diskAction' " + otherButtonClasses + ">File</button>" +
                                                "</div>" +
                                                "<div class='col-lg-2'>" +
                                                "</div>"
                                              : "<div class='col-lg-4'>" +
                                                "</div>" +
                                                "<div class='col-lg-4'>" +
                                                  "<button data-action='mount' data-diskId='" + diskId + "' type='button' class='btn btn-block btn-success diskAction'>Mount</button>" +
                                                "</div>" +
                                                "<div class='col-lg-4'>" +
                                                "</div>";                                                


  var diskElementTop = "<div class='col-md-6'>" +
                          "<div class='panel panel-primary'>" +
                            "<div class='panel-heading'>" +
                              "<strong>" + diskData.name + "</strong> " +
                              "<strong class='disk_mountPoint' data-diskName='" + diskData.name + "' data-diskId='" + diskId + "' " + elementVisibility + "> --> " + diskData.mountPoint + "</strong> " +
                              "<span class='load_icon' data-diskName='" + diskData.name + "' data-diskId='" + diskId + "'><i class='fa fa-spinner fa-pulse fa-fw load_icon_disks'> </i></span>" +

                            "</div>" +

                            "<div class='panel-body diskDataContainer' data-diskId='" + diskId + "' data-diskName='" + diskData.name + "'>";                     

  var diskElementCore =       "<div class='col-lg-12 diskInfoContainer' data-diskId='" + diskId + "' "+ menuVisibility + ">" +
                                
                                "<div class='diskInfoContainer_Background col-lg-12' data-diskId='" + diskId + "'></div>" +
                                
                                "<div class='modal-content diskInfoContainer_inner col-lg-12'>" +

                                  "<div class='modal-body'>" +
                                    "<button type='button' class='diskAction close' data-diskId='" + diskId + "' data-action='close' "+ elementVisibility + ">×</button>" +
                                    "<div class=''>" +
                                      "<strong>Label: </strong>" +
                                      "<span>" +diskData.label + "</span>"+
                                    "</div>"+

                                    "<div class='' " + elementVisibility + ">" +
                                      "<strong>MountPoint: </strong>" +
                                      "<span>" +diskData.mountPoint + "</span>"+
                                    "</div>"+
                                    
                                    "<div class=''>" +
                                      "<strong>Uuid: </strong>" +
                                      "<span>" +diskData.uuid + "</span>"+
                                    "</div>"+

                                    "<div class=''>" +
                                      "<strong>FS: </strong>" +
                                      "<span>" +diskData.fileSystem + "</span>"+
                                    "</div>"+

                                    "<div class=''>" +
                                      "<strong>Size: </strong>" +
                                      "<span>" +diskData.size + "</span>"+
                                    "</div>"+

                                    "<div class='' " + elementVisibility + ">" +
                                      "<strong>Used: </strong>" +
                                      "<span>" +diskData.used + "</span>"+
                                    "</div>"+ 

                                    "<div class='' " + elementVisibility + ">" +
                                      "<strong>Free: </strong>" +
                                      "<span>" +diskData.available + "</span>"+
                                    "</div>"+

                                    "<div class='errorField' data-diskId='" + diskId + "'>" +
                                      "<strong class='errorField' data-diskId='" + diskId + "'></strong>" +
                                    "</div>"+ 

                                  "</div>" +

                                  "<div class='modal-footer'>" +
                                    "<div class='col-lg-12'>" +                     
                                      actionsButtons +                                                                      
                                    "</div>"+

                                    
                                  "</div>" +

                                "</div>"+     

                              "</div>"+
                              
                              "<div class='col-md-12 flot-chart diskQuotaContainer' data-diskId='" + diskId + "'>" +
                                "<canvas id='disk_" + diskId + "'></canvas>" +
                              "</div>";
  
  var diskElementBottom =   "</div>" +
                          
                          "</div>" +
                        "</div>";

  if(update){
    // $(".disk_name[data-diskName='" + diskData.name + "']").attr("data-diskId", diskId);
    
    var diskContainer = $("div.diskDataContainer[data-diskName='" + diskData.name + "']");
    diskContainer.attr("data-diskId", diskId);
    diskContainer.empty();
    diskContainer.append(diskElementCore);

    var mountLabel = $(".disk_mountPoint[data-diskName='" + diskData.name + "']");
    mountLabel.attr("data-diskId", diskId);
    if(isDiskMounted){
      mountLabel.css('display', 'inline').text(' --> ' + diskData.mountPoint );
    } else{
      mountLabel.css('display', 'none').text('');
    }

    diskContainer = null;
  } else {
    disksContainer.append(diskElementTop + diskElementCore + diskElementBottom);
    disksContainer = null;
  }
  
  var ctx = document.getElementById("disk_" + diskId).getContext('2d');

  var freeChartColor = isDiskMounted == true ? '#2FA869': '#c0c0c0';
  var usedChartColor = isDiskMounted == true ? '#FF4C39': '#c0c0c0';

  var data = {
    datasets: [{
      data: [
        diskFreePercent,
        diskUsedPercent
      ],
      backgroundColor: [
        freeChartColor,
        usedChartColor
      ],
    }],
    labels: [
        "Free",
        "Used"                              
    ]
  };

  var diskPieChart = new Chart(ctx,{
    type: 'doughnut',
    data: data,
    options: graphOptions
  });

  if(isDiskMounted) {
    $('div.diskQuotaContainer[data-diskId=' + diskId + ']').unbind();
    $('div.diskQuotaContainer[data-diskId=' + diskId + ']').click(function (e) {
      var disk = e.currentTarget.getAttribute("data-diskId");
      
      $('strong.errorField[data-diskId=' + diskId + ']').text('');
      $('div.errorField[data-diskId=' + diskId + ']').hide();
      
      $('div.diskInfoContainer[data-diskId=' + disk + ']').fadeIn('fast');

    });

    $('div.diskInfoContainer_Background[data-diskId=' + diskId + ']').unbind();
    $('div.diskInfoContainer_Background[data-diskId=' + diskId + ']').click(function (e) {
      var disk = e.currentTarget.getAttribute("data-diskId");
      
      $('strong.errorField[data-diskId=' + diskId + ']').text('');
      $('div.errorField[data-diskId=' + diskId + ']').hide();
      
      $('div.diskInfoContainer[data-diskId=' + disk + ']').fadeOut('fast');
    });
  }
  
  $('button.diskAction[data-diskId=' + diskId + ']').unbind();
  $('button.diskAction[data-diskId=' + diskId + ']').click(function (e) {
    var disk = e.currentTarget.getAttribute("data-diskId");
    var action = e.currentTarget.getAttribute("data-action");

    $('button.diskAction[data-diskId=' + diskId + ']').prop('disabled', true);

    switch (action) {
      case 'fileManager':
        window.open("https://" + serverBaseUrl + serverRootPath + "/fileManager#" + CacheDiskData[disk].mountPoint );
        $('button.diskAction[data-diskId=' + diskId + ']').prop('disabled', false);          
        break;
      case 'mount':
        MountService(action, CacheDiskData[disk]);
        break;
      case 'umount':
        MountService(action, CacheDiskData[disk]);
        break;
      case 'close':
        $('div.diskInfoContainer[data-diskId=' + disk + ']').fadeOut('fast');
        $('button.diskAction[data-diskId=' + diskId + ']').prop('disabled', false);
        break;
      default:
        break;
    }

    var errorField = $('div.errorField[data-diskId=' + diskId + ']');
    if(errorField.is(':visible')){
      errorField.slideUp('fast');
      $('strong.errorField[data-diskId=' + diskId + ']').text('');
    }
  });

  $(".load_icon[data-diskName='" + diskData.name + "']").attr("data-diskId", diskId).removeClass('waiting_ajax');

}

function MountService(action, diskData){

  var loader = $('span.load_icon[data-diskId=' + diskData.id + ']').addClass('waiting_ajax');

  var dataJSON = {
    action: "disk_" + action,
    value: {
      name: diskData.name,
      uuid: diskData.uuid,
      mountPoint: diskData.mountPoint
    }
  }

  $.ajax({
    type: "POST",
    url: "https://" + serverBaseUrl + serverRootPath + "/API/WS_Disks.php",
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {
      if( msg.code == 0 ) {
        CacheDiskData = msg.response;

        for( i = 0; i < msg.response.length; i++ ) {
          if(msg.response[i].name == diskData.name)
            break;
        }     

        GenerateDiskElement(msg.response[i], null, true);
      } else {
        console.log(msg.code);
        $('span.load_icon[data-diskId=' + diskData.id + ']').removeClass('waiting_ajax');
        
        $('strong.errorField[data-diskId=' + diskData.id + ']').text(msg.response);
        $('div.errorField[data-diskId=' + diskData.id + ']').slideDown('fast');
        
        $('button.diskAction[data-diskId=' + diskData.id + ']').prop('disabled', false);
      }
    },
    error: function(msg) {
      console.log("FATAL_ERROR");
      $('span.load_icon[data-diskId=' + diskData.id + ']').removeClass('waiting_ajax');
      $('button.diskAction[data-diskId=' + diskData.id + ']').prop('disabled', false);
      
    },
    complete: function() {
    }
  });
  

  
  
}

function LoadKnowndisks(){
   var dataJSON = {
    action: "get_known_disks"
  }

  $.ajax({
    type: "POST",
    url: "https://" + serverBaseUrl + serverRootPath + "/API/WS_Disks.php",
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {
      if( msg.code == 0 ) {

        RenderKnownDisks(msg.response);
        
      } else {
        console.log("ERROR");
        $('#knownDisks_loading').fadeOut('fast');

      }
    },
    error: function(msg) {
      console.log("FATAL_ERROR");
      $('#knownDisks_loading').fadeOut('fast');
      
    },
    complete: function() {

    }
  });

}

function RenderKnownDisks(knownDisks){

    var currentDisk = knownDisks[i];

    var newTable = "";
    var items = 0;
    var knownDisks = knownDisks.slice();
    KnownDisksUuid = [];
    for( i = 0; i < knownDisks.length; i++ ) {

      var thisResp = knownDisks[i];
      var persistentF = thisResp['persistentFolder'] == 1 ? 'checked' : ''; 

      newTable += '<tr data-local-id="' + i + '">\n';
      // newTable += '	<td>'+ parseInt(i + 1 )+ '</td>\n';
      newTable += '	<td><span>'+ thisResp['entryId'] + '</span></td>\n';      
      newTable += '	<td><span>'+ thisResp['uuid'] + '</span></td>\n';
      newTable += '	<td><span>'+ thisResp['mountPoint'] + '</span></td>\n';
      newTable += '	<td><input disabled type="checkbox" ' + persistentF + ' ></td>\n';
                                                    
      newTable += '</tr>';
      items = items + 1;
      KnownDisksUuid[thisResp['entryId']] = thisResp['uuid'];
    }

    $("#knownDisksTable tbody").empty();
    $("#knownDisksTable tbody").html( newTable );

    ManageTableSorterPagerRender('knownDisksTable', items, true);
    InitializeTableEdit('knownDisksTable');

    

    var addDisk = '<tr data-local-id="' + (i+1) + '">\n';
    addDisk += '	<td><span>New</span></td>\n';
    addDisk += '	<td><input type="text" list="connectedDisks" data-attribute="uuid" class="tabledit-input form-control input-sm addDiskInput"></input>';
    addDisk += '<datalist id="connectedDisks">' + GetAvailableDiskForAdd(true) + '</datalist></td>\n';
    addDisk += '	<td><input data-attribute="mountPoint" class="tabledit-input form-control input-sm addDiskInput"></input></td>\n';
    addDisk += '	<td><input data-attribute="persistentFolder" class="addDiskInput" type="checkbox"></td>\n';
    addDisk += '	<td><button id="btnAddKnownDisk" class="btn btn-block btn-success">Add</button></td>\n';
    addDisk += '</tr>';

    $("#knownDisksTable tbody").append( addDisk );

    $('#btnAddKnownDisk').unbind();
    $('#btnAddKnownDisk').click(function(e){
      AddKnownDisk();
    });

    $('#knownDisks_loading').fadeOut('fast');
}

function InitializeTableEdit(table){
  $('#' + table).Tabledit({
    url: "https://" + serverBaseUrl + serverRootPath + "/API/WS_Disks.php",
    ajaxJsonFormat: true,
    restoreButton: false,
    mutedClass: 'success-fadeOut',
    columns: {
        identifier: [0, 'entryId'],
        editable: [[1, 'uuid'], [2, 'mountPoint']]
    },
    buttons: {
        edit: {
          action: 'update_known_disk'
        },
        delete: {
          action: 'delete_known_disk'
        }
    },
    onSuccess: function(data, textStatus, jqXHR, action) {

      if( data.code != 0 ) {
        console.log("ERROR");
        console.log(data.response);
        
      } else {

        //fix the knownDisks array without other web call
        if(action == 'update_known_disk'){
          KnownDisksUuid[data.response.entryId] = data.response.uuid;
        } else{
          KnownDisksUuid.splice(data.response.entryId);
        }

        $('#connectedDisks').html(GetAvailableDiskForAdd(true));
      }

    },
    onFail: function(jqXHR, textStatus, errorThrown) {
        console.log('onFail(jqXHR, textStatus, errorThrown)');
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
    }

  });
}

function AddKnownDisk(){
  $('#knownDisks_loading').fadeIn('fast');
  
  var disk = {};
  
  $( ".addDiskInput" ).each(function( index ) {
    var value = $( this ).val();
    var attribute = $( this ).attr('data-attribute')

    if(attribute == 'persistentFolder'){
      value = $(this)[0].checked == true ? 1 : 0;
    }

    disk[attribute] = value;
  });

  var dataJSON = {
    action: "add_known_disk",
    value: disk
  }

  $.ajax({
    type: "POST",
    url: "https://" + serverBaseUrl + serverRootPath + "/API/WS_Disks.php",
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {
      if( msg.code == 0 ) {

        LoadKnowndisks();
        
      } else {
        console.log("ERROR");
        $('#knownDisks_loading').fadeOut('fast');

      }
    },
    error: function(msg) {
      console.log("FATAL_ERROR");
      $('#knownDisks_loading').fadeOut('fast');
      
    },
    complete: function() {

    }
  });


}

function GetAvailableDiskForAdd(asOption){

  var connectedDisks = [];
  for( i = 0; i < CacheDiskData.length; i++ ) {
    var currentDisk = CacheDiskData[i];
    if(currentDisk.mountPoint == null && jQuery.inArray( currentDisk.uuid, KnownDisksUuid ) < 0 ){
      var diskLabel = currentDisk.label != null ? currentDisk.label : currentDisk.uuid;
      if(asOption == true){
        connectedDisks += '<option value="' + currentDisk.uuid + '">' + diskLabel + '</option>'
      } else{
        //add code if other formats needed
      }
      
    }
  }
  
  return connectedDisks;

}