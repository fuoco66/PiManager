
<script type="text/javascript" src="../js/copyManager.js"></script>

<div id="copyInfoContainer" style="display: none;" class="col-md-12">

  <div class="col-md-12">
    <p>
      <strong id="copyInfo_sourceFileName">copyInfo_sourceFileName</strong>
      <span id="copyInfo_percentage" class="pull-right text-muted">40%</span>
    </p>
    <div class="progress progress-striped">
      <div id="copyInfo_ProgressBar" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0" style="width: 0%">
          <span id="copyInfo_percentage_hidden" class="sr-only"></span>
      </div>
    </div>
  </div>

  <div class="col-md-12 pathContainer">
      <strong>Source: </strong>
      <span id="copyInfo_sourceFullPath"></span>
  </div>

  <div class="col-md-12 pathContainer">
      <strong>Destination: </strong>
      <span id="copyInfo_destinationFullPath"></span>
  </div>

  <div class="">
    <div class="col-md-12">
      <strong>Speed: </strong>
      <span id="copyInfo_speed">0</span>
    </div>
    <div class="col-md-12">
      <strong>Sent: </strong>
      <span id="copyInfo_sentData">0</span>
    </div>
    <div class="col-md-12">
      <strong >ETA: </strong>
      <span id="copyInfo_eta"> 0:00:00</span>
    </div>
  </div>

  <div class="centered-full col-md-12 col-xs-12">
    <button id="abortCopy" type="button" class="btn btn-danger btn-block" data-preventClosure="true">Cancel</button>
  </div>

</div>
<div id="copyInfo_noCopy" class="col-md-6 col-xs-12 centered-full">
  <p>
    <strong>No copy in progress</strong>
  </p>
</div>