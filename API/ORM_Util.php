<?php

  class ORM_Util {

    // Costruttore
  	public function __construct(){ }
  
		public function InitPiManager($initDb, $forceLoad, &$errorCode, &$errorMessage) {
			
			$dbConn = null;
			if($initDb){
				$this->InitDb($dbConn, $forceLoad, $errorCode, $errorMessage);
				$dbConn->close();
			}

			
			return null;

    }

		private function InitDb(&$database, $forceLoad, &$errorCode, &$errorMessage){

			$servername = "localhost";
			$username = "root";
			$password = "f4e7889@@@";
			$dbName = 'PiManager';

			$database = new mysqli($servername, $username, $password);

			if ($database->connect_error) {
				$errorCode = -3;
        $errorMessage = "NO_CONNECTION";
        return null;
			}

			//create Db
			$database->query("CREATE DATABASE IF NOT EXISTS " . $dbName . ";");

			$database->select_db($dbName);
			//connect to the correct db
			// $database = new mysqli($servername, $username, $password, $dbName);

			if ($database->connect_error) {
				$errorCode = -4;
        $errorMessage = "NO_CONNECTION";
        return null;
			}

			$sqlStr = "SELECT setValue
								 FROM systemSettings 
								 WHERE setKey = 'init' AND
								 			 setSection = 'system'";

			$alreadyInitialized = $database->query($sqlStr);

			if($alreadyInitialized != false && $alreadyInitialized->num_rows > 0 && !$forceLoad ){
				$row = $alreadyInitialized->fetch_assoc();

				if($row["setValue"] ==1 ){
					return $alreadyInitialized;
				}
			}

			//init tables
			//knownDisks table
			$sqlStr = "CREATE TABLE IF NOT EXISTS knownDisks (
										uuid VARCHAR(255) NOT NULL,
										mountPoint VARCHAR(255) NOT NULL,
										entryId int NOT NULL AUTO_INCREMENT,
										persistentFolder int NOT NULL,
										dateUpdate DATETIME NOT NULL,
										PRIMARY KEY(uuid),
										KEY entryId (entryId))";

			$res = $database->query($sqlStr);

			//system global settings 
			$sqlStr = "CREATE TABLE IF NOT EXISTS systemSettings (
										setKey VARCHAR(255) NOT NULL, 
										setSection VARCHAR(255) NOT NULL, 
										setValue VARCHAR(255) NOT NULL, 
										PRIMARY KEY (setKey, setSection));";

			$res = $database->query($sqlStr);

			//system process
			$sqlStr = "CREATE TABLE IF NOT EXISTS systemProcess (
										id int(11) NOT NULL AUTO_INCREMENT,
										commonName varchar(255) NOT NULL,
										formattedName varchar(255) NOT NULL,
										taskName varchar(255) NOT NULL,
										serviceName varchar(255) NOT NULL,
										startCommand varchar(255) DEFAULT NULL,
										stopCommand varchar(255) DEFAULT NULL,
										restartCommand varchar(255) DEFAULT NULL,
										startBtn int(11) DEFAULT 1,
										stopBtn int(11) DEFAULT 1,
										restartBtn int(11) DEFAULT 1,
										PRIMARY KEY(taskName),
										KEY entryId (id))";

			$res = $database->query($sqlStr);	

			$errorMessage = $database->error;														

			$knownDisks = array(
				array(
					"uuid" => "56bdb4f7-7bbb-4c96-a3af-ef83f5c95d77",
					"persistentFolder" => 1,
					"mountPoint" => "/media/Data"),
				array(
					"uuid" => "A00C79C40C7995CE",
					"persistentFolder" => 1,
					"mountPoint" => "/media/Seagate"),
				array(
					"uuid" => "2C2A0B892A0B4EF0",
					"persistentFolder" => 1,
					"mountPoint" => "/media/ScreenPlay")
			);

			$sqlStr = "INSERT INTO knownDisks (uuid, mountPoint, persistentFolder, dateUpdate) VALUES";
			
			foreach ($knownDisks as $value) {
				$uuid = "'" . $database->real_escape_string($value["uuid"]) . "'";
				$mountPoint = "'" . $database->real_escape_string($value["mountPoint"]) . "'";

				$sqlStr .= " (" . $uuid . ", " . $mountPoint . ", " . $value["persistentFolder"] . ", NOW()),";

			}

			$sqlStr = rtrim($sqlStr, ',');
			$sqlStr .= ";";

			$res = $database->query($sqlStr);


			$systemProcess = array(
				array(
					"commonName" => "transmission",
					"formattedName" => "Transmission",
					"taskName" => "transmission",
					"serviceName" => "transmission-daemon",
					"startCommand" => "NULL",
					"stopCommand" => "NULL",
					"restartCommand" => "NULL",
					"startBtn" => 1,
					"stopBtn" => 1,
					"restartBtn" => 1 ),
				array(
					"commonName" => "samba",
					"formattedName" => "Samba",
					"taskName" => "smbd",
					"serviceName" => "smbd",
					"startCommand" => "NULL",
					"stopCommand" => "NULL",
					"restartCommand" => "NULL",
					"startBtn" => 1,
					"stopBtn" => 1,
					"restartBtn" => 1 ),
				array(
					"commonName" => "dummy",
					"formattedName" => "Dummy",
					"taskName" => "dummy",
					"serviceName" => "dummy",
					"startCommand" => "NULL",
					"stopCommand" => "NULL",
					"restartCommand" => "NULL",
					"startBtn" => 1,
					"stopBtn" => 1,
					"restartBtn" => 1 ),
				array(
					"commonName" => "apache",
					"formattedName" => "Apache",
					"taskName" => "apache2",
					"serviceName" => "apache2",
					"startCommand" => "NULL",
					"stopCommand" => "NULL",
					"restartCommand" => "NULL",
					"startBtn" => 0,
					"stopBtn" => 0,
					"restartBtn" => 1 )
			);

			$sqlStr = "INSERT INTO systemProcess (commonName, formattedName, taskName, serviceName, startBtn, stopBtn, restartBtn) VALUES";
			
			foreach ($systemProcess as $value) {
				$commonName = "'" . $database->real_escape_string($value["commonName"]) . "'";
				$formattedName = "'" . $database->real_escape_string($value["formattedName"]) . "'";
				$taskName = "'" . $database->real_escape_string($value["taskName"]) . "'";
				$serviceName = "'" . $database->real_escape_string($value["serviceName"]) . "'";

				$sqlStr .= " (" . $commonName . ", " . $formattedName . ", " . $taskName . ", " . $serviceName . ", " . $value["startBtn"] . ", " . $value["stopBtn"] . ", " . $value["restartBtn"] . "),";

			}

			$sqlStr = rtrim($sqlStr, ',');
			$sqlStr .= ";";

			$res = $database->query($sqlStr);

			$sqlStr = "INSERT INTO systemSettings (setKey, setSection, setValue)
																			VALUES('init', 'system', '1')";

			// if everything went well, sets the init field to true																
			if($res != false){
				$res = $database->query($sqlStr);
				
				if($res == false){
					$sqlStr = "UPDATE systemSettings SET setValue = '1' 
											WHERE setKey = 'init' AND setSection = 'system'";	
					$res = $database->query($sqlStr);
																				
				}
				
			}

		}

		public function GetInitData(&$errorCode, &$errorMessage){

		}

		public function GetDbConnection(&$errorCode, &$errorMessage){
			$servername = "localhost";
			$username = "root";
			$password = "f4e7889@@@";
			$dbName = 'PiManager';

			$database = new mysqli($servername, $username, $password, $dbName);
			
			if ($database->connect_error) {
				$errorCode = -5;
				$errorMessage = "NO_CONNECTION";
				return null;
			}

			return $database;
		}

    public function VerifyWsParameters(&$errorCode, &$errorMessage) {
      $postdata = file_get_contents("php://input");

      //Verify parameters, JSON parameter
      if ($postdata == null) {
        $errorCode = -1;
        $errorMessage = "NO_PARAM";
        return null;
      }

      $JSON = json_decode($postdata);

      if (!isset($JSON)) {
        $errorCode = -2;
        $errorMessage = "INVALID_PARAM";
        return null;
      }

       return $JSON;

    }

    public function ManageWebServiceResponse($return_object, &$errorCode, &$errorMessage){

      //verify if there is an error
      if($errorCode != 0){
        return json_encode(array(
                                'code'=>$errorCode,
                                'response'=>$errorMessage));
      }

      //return response for correct reply
      return json_encode(array(
                              'code'=> 0,
                              'response' => $return_object));

    }

		/**
		* Executes the shell command and gets stdout and stderr streams
		*
		* @see http://www.php.net/manual/en/function.proc-open.php
		* @param string $cmd - command to be executed
		* @param string $cwd - folder
		* @param array $env - options
		* @return array()
		**/
    public function shexec($cmd, $cwd = './', $env = array()){
        $return_value = array(
             "exit"   => 1,       // exit 0 on ok
             "stdout" => "",      // output of the command
             "stderr" => "",      // errors during execution
        );

        $descriptorspec = array(
        0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
        1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
        2 => array("pipe", "w")   // stderr is a pipe
        );

        // $process = proc_open(escapeshellcmd($cmd), $descriptorspec, $pipes, $cwd, $env);         
        $process = proc_open($cmd, $descriptorspec, $pipes, $cwd, $env);
        // $pipes now looks like this:
        // 0 => writeable handle connected to child stdin
        // 1 => readable handle connected to child stdout
        // 2 => readable handle connected to child stderr

        if (false === is_resource($process))
        {
            //echo("Sys::shexec() Error on proc_open, shell command \"$cmd\"");
        }
        else
        {
            $return_value['stdout'] = stream_get_contents($pipes[1]);
            $return_value['stderr'] = stream_get_contents($pipes[2]);

            fclose($pipes[0]);
            fclose($pipes[1]);
            fclose($pipes[2]);

            // It is important that you close any pipes before calling
            // proc_close in order to avoid a deadlock
            $return_value['exit'] = proc_close($process);
        }

        if(trim($return_value['stderr']) !== "") {
            //echo("Sys::shexec() \n\"$cmd\"\nERROR:\n" . $return_value['stderr']);
        }

        if(trim($return_value['stdout']) !== "") {
            //echo("Sys::shexec() \n\"$cmd\"\nOUTPUT:\n" . $return_value['stdout']);
        }

        return $return_value;

    } // END FUNCTION shexec()

  
  
  }

?>