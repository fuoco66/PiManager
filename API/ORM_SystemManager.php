<?php

  class ORM_SystemManager {

    // Costruttore
  	public function __construct(){ }
  
		public function GetProcessStatus($processList, &$errorCode, &$errorMessage) {
      $UtilObject = new ORM_Util();
      $dbConn = $UtilObject->GetDbConnection($errorCode, $errorMessage);
      
      if(!isset($dbConn)){
        $errorCode = -3001;
				$errorMessage = "NO_CONNECTION";
				return null;
      }

      $processList_query = "";

      foreach ($processList as $value) {
        $processList_query .= "commonName = '" . $value . "' OR ";
      }

      $processList_query = rtrim($processList_query, 'OR ');

      $sqlStr = "SELECT *
            FROM systemProcess WHERE " . $processList_query;
      
      $queryRes = $dbConn->query($sqlStr);

      $processArray = array();      
      while ($row = $queryRes->fetch_assoc()) {

        $output = $UtilObject->shexec("../systemManager/script/systemManager -p " . escapeshellarg($row['taskName']) . " null");

          if($output["stderr"] != "") {
            $errorCode = -3002;
            $errorMessage = $output["stderr"];
            return null;
          }
          $formattedObj = json_decode($output['stdout']);
          $formattedObj->commonName = $row['commonName'];
          $formattedObj->formattedName = $row['formattedName'];
          $formattedObj->processId = intval($row['id']);
          $formattedObj->enabledFunctions = array(
            'start' => intval($row['startBtn']),
            'stop' => intval($row['stopBtn']),
            'restart' => intval($row['restartBtn']),
          );

          array_push($processArray, $formattedObj);
            
      }

      return $processArray;

    }

    public function ProcessAction($value, &$errorCode, &$errorMessage) {
      $UtilObject = new ORM_Util();
      $dbConn = $UtilObject->GetDbConnection($errorCode, $errorMessage);
      $processId = $value->processId;
      $processAction = $value->function;

      if(!isset($dbConn)){
        $errorCode = -3003;
				$errorMessage = "NO_CONNECTION";
				return null;
      }

      $sqlStr = "SELECT *
                 FROM systemProcess WHERE id=" . $processId;
      
      $queryRes = $dbConn->query($sqlStr);

      $processArray = array(); 

      while ($row = $queryRes->fetch_assoc()) {
        // if there's not a custom command in the db uses the default syntax (-ssD)
        $command = $row[ $processAction . 'Command'] != null ? '-ssC ' . $row[$processAction . 'Command'] : '-ssD ' . escapeshellarg($row['serviceName']) . ' ' . $processAction;

        $output = $UtilObject->shexec("../systemManager/script/systemManager " . $command);

        // check if error in action execution
        if($output["stderr"] != "") {
          $errorCode = -3004;
          $errorMessage = $output["stderr"];
          return null;
        }

        // check task status

        $output = $UtilObject->shexec("../systemManager/script/systemManager -p " . escapeshellarg($row['taskName']) . " null");

        if($output["stderr"] != "") {
          $errorCode = -3005;
          $errorMessage = $output["stderr"];
          return null;
        }
        $formattedObj = json_decode($output['stdout']);
        $formattedObj->commonName = $row['commonName'];
        $formattedObj->formattedName = $row['formattedName'];
        $formattedObj->processId = intval($row['id']);
        $formattedObj->enabledFunctions = array(
            'start' => intval($row['startBtn']),
            'stop' => intval($row['stopBtn']),
            'restart' => intval($row['restartBtn']),
          );

      }

      return $formattedObj;

    }

  }

?>