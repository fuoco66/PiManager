var mediaDirectoryRoot = "/media";
var downloadDirectory = "/media/Data/Downloads";
// var seagateRoot = "/media/Seagate/";
var CacheCurrentDirectory = null;
var GlobalCurrentDirectory = null;
var BackDirectory = null;
var ForwardDirectory = [];
var SelectionMode = 0;
var SelectedObjectArray = [];
var GlobalPaste = null;

$( document ).ready(function() {

  var path = $(location).attr('href').split("#")[1];
  path = (path == "" || path == null) ?  mediaDirectoryRoot : path;

  LoadFileList(path);

  $('.toolbarButton').on('click', function(){
    var buttonFunction =($(this).attr("data-function"));

    switch (buttonFunction) {
      case 'back':
        BackClicked();
        break;
      case 'forward':
        ForwardClicked();
        break;
      case 'paste':
        PasteObject('pasteRoot', null);
        break;
      case 'delete':
        DeleteObjects();
        break;
      case 'refresh':
        LoadFileList(GlobalCurrentDirectory);
      break;
      case 'selectorToggle':
        if( SelectionMode == 0 ){
          SelectionMode = 2;
          $(this).removeClass('notActive');
        } else {
          SelectionMode = 0;
          $(this).addClass('notActive');
        }

      default:
        return;
    }

  });

  $( document ).keydown(function( event ) {

    // if(SelectionMode == 0){
      switch (event.which) {
        case 16:
          SelectionMode = 1; //"shift";
          break;
        case 17:
          SelectionMode = 2; //"ctrl";
          break;
        case 27:
          UnselectAll();
          break;
        default:
          UnselectAll();
      }
    // }
    console.log(SelectionMode);

  }).keyup(function( event ) {
    SelectionMode = 0;
  });

});

function GetFolderPath(name){
  var res = null;

  switch (name) {
    case ("root"):
      res = mediaDirectoryRoot;
      break;

    case ("downloads"):
      res = downloadDirectory;

      break;
    default:
      res = mediaDirectoryRoot;
      break;
  }
  return res;
}

function LoadFileList(currentDirectory){
  UnselectAll();

  var dataJSON = {
    action: "list_files",
    path: currentDirectory
  }

  $("#directoryOpLoader").addClass('waiting_ajax');

  $.ajax({
    type: "POST",
    url: "https://" + serverBaseUrl + serverRootPath + "/API/WS_Files.php",
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {
      if( msg.code == 0 ) {

        CacheCurrentDirectory = null;
        CacheCurrentDirectory = msg.response;
        GlobalCurrentDirectory = currentDirectory;

        RenderFileList(msg.response)
      } else {
        console.log("ERROR");
        $("#directoryOpLoader").removeClass('waiting_ajax');
      }
    },
    error: function(msg) {
      console.log("FATAL_ERROR");
      $("#directoryOpLoader").removeClass('waiting_ajax');
    },
    complete: function() {

    }
  });

}

function RenderFileList(directoryData){
  // fileTable
  $("#currentFolderName").html( GlobalCurrentDirectory );

  if(directoryData.canGoBack){
    $("#toolbarButton_Back").removeClass("disabled");
  } else {
    $("#toolbarButton_Back").addClass("disabled");
  }

  if(ForwardDirectory.length > 0){
    $("#toolbarButton_Forward").removeClass("disabled");
  } else {
    $("#toolbarButton_Forward").addClass("disabled");
  }

  var newTable = "";
  var items = 0;
  var filesArray = directoryData.filesList.slice();
  for( i = 0; i < filesArray.length; i++ ) {

    CacheCurrentDirectory['filesList'][i]['id'] = i;
    thisResp = filesArray[i];

    newTable += '<tr class="directoryObj" data-local-id="' + i + '">\n';
    // newTable += '	<td>'+ parseInt(i + 1 )+ '</td>\n';
    newTable += '	<td><span>'+ thisResp['name'] + '</span></td>\n';
    newTable += '	<td><span>'+ thisResp['type'] + '</span></td>\n';
    newTable += '	<td data-column-order="'+ thisResp['sizeRaw'] +'" ><span>'+ thisResp['sizeFormatted'] + '</span></td>\n';
    newTable += '	<td data-column-order="'+ thisResp['timeRaw'] +'" ><span>'+ thisResp['timeFormatted'] + '</span></td>\n';
    newTable += '</tr>';
    items = items + 1;
  }

  $("#fileTable tbody").empty();
  $("#fileTable tbody").html( newTable );

  ManageTableSorterPagerRender('fileTable', items, true);

  $("#directoryOpLoader").removeClass('waiting_ajax');

  $('.directoryObj').off('click');
  $('.directoryObj').on('click', function(){

    if(SelectionMode == 0 ){
      var obj = CacheCurrentDirectory['filesList'][ $(this).attr('data-local-id') ];
      UnselectAll();
      SelectOneElement( obj,$(this));
    } else {
      ElementClicked( $(this) );
    }

  });

  $('.directoryObj').off('dblclick');
  $('.directoryObj').on('dblclick', function(){
    if(Object.keys(SelectedObjectArray).length <= 1){
      ElementClicked( $(this) );
    } else{
      UnselectAll();
    }

  });

  $(".directoryObj").on("taphold",function(e){
    e.preventDefault();
    $('.directoryObj').contextMenu();
  });

  $.contextMenu({
      selector: '.directoryObj',
      animation: {duration: 100, show: 'fadeIn', hide: 'fadeOut'},
      callback: function(key, options) {

        var obj = CacheCurrentDirectory['filesList'][ this.attr('data-local-id') ];
        ContextMenuCallback( key, this , obj );
        obj = null;

      },
      // autoHide: true,
      items: {
          // "edit": {name: "Edit", icon: "edit"},
          // "cut": {name: "Cut", icon: "cut"},
          "download": {name: "Download", icon: "edit", disabled: function(key, opt){ return IsDownloadEnabled(key, this); }},
          "copy": {name: "Copy", icon: "copy", disabled: function(key, opt){ return IsCopyEnabled(key, this); }},

          "pasteFolder": {
                "name": "Paste",
                "items": {
                    "pasteRoot": {name: "Paste", icon: "paste", disabled: function(key, opt){ return IsPasteEnabled(key, this); } },
                    "pasteChild": {name: "Paste In Selected", icon: "paste", disabled: function(key, opt){ return IsPasteEnabled(key, this); } }
                }
            },
          "delete": {name: "Delete", icon: "delete"},
          "sep1": "---------",
          "quit": {name: "Quit", icon: function(){
              return 'context-menu-icon context-menu-icon-quit';
          }}
      },
      events: {
        show : function(options){
          if(Object.keys(SelectedObjectArray).length <= 1){
            UnselectAll();
            var obj = CacheCurrentDirectory['filesList'][ this.attr('data-local-id') ];
            SelectOneElement( obj, this);
            obj = null;
          }
        },
        hide : function(options){
          // UnselectAll();
        }
      }

  });

}

function IsCopyEnabled(action, element){

  if(RefreshRunning){
    return true;
  }

  if(Object.keys(SelectedObjectArray).length == 1){
    var obj = SelectedObjectArray[ element.attr('data-local-id') ];
    if(obj.objClass == 'dir'){
      obj = null;
      return true;
    }
    obj = null;

    return false;
  }

  return false;
}

function IsDownloadEnabled(action, element){

  if(Object.keys(SelectedObjectArray).length == 1 ){
    var obj = SelectedObjectArray[ element.attr('data-local-id') ];
    if(obj.objClass == 'dir'){
      obj = null;
      return true;
    }
    obj = null;

    return false;
  }

  return true;
}

function IsPasteEnabled(action, element){

  if(RefreshRunning){
    return true;
  }

  if(GlobalPaste != null && Object.keys(SelectedObjectArray).length <= 1 ){

    if( action == 'pasteChild'){
      var obj = SelectedObjectArray[ element.attr('data-local-id') ];
      if(obj.objClass != 'dir'){
        obj = null;
        return true;
      }
      obj = null;

    }

    return false;
  }

  return true;
}

function ContextMenuCallback( action, currentElement, currentObj ){

  switch (action) {
    case 'download':
      DownloadFile(currentElement, currentObj);
      break;
    case 'copy':
      GlobalPaste = currentObj; //consider if somethig else to do
      $('#toolbarButton_Paste').removeClass('disabled');
      break;
    case 'pasteRoot':
      PasteObject( action, currentObj );
      break;
    case 'pasteChild':
      PasteObject( action, currentObj );
      break;
    case 'delete':
      DeleteObjects();
      break;
    default:

  }
}

function DeleteObjects( ){

  if (Object.keys(SelectedObjectArray).length < 1){
    console.log('nothing selected');
    return;
  }

  var objectsToDelete = [];
  var confirmMessage = "";

  jQuery.each(Object.keys(SelectedObjectArray), function(index, associativeIndex) {
    var currentElement = SelectedObjectArray[associativeIndex];
    var obj = {
	       name: currentElement.name,
         extn: currentElement.extn,
         objClass: currentElement.objClass,
	       completePath: currentElement.completePath,
         dataLocalId: parseInt(associativeIndex)
    };
    objectsToDelete.push(obj);
    confirmMessage += obj.name + "\n";

    $(".directoryObj[data-local-id=" + associativeIndex + "]").removeClass("rowSelected");
    $(".directoryObj[data-local-id=" + associativeIndex + "]").addClass("warning");
  });


  if (!confirm('Are you sure you want to delete:\n' + confirmMessage )) {
    console.log("abort");

    jQuery.each(Object.keys(SelectedObjectArray), function(index, associativeIndex) {
      $(".directoryObj[data-local-id=" + associativeIndex + "]").removeClass("warning");
    });
    UnselectAll();
    return;
  }

  UnselectAll();

  var dataJSON = {
    action: "delete_files",
    value: {
      files: objectsToDelete }
  };

  // $('#toolbarButton_Paste').addClass('disabled');
  $("#directoryOpLoader").addClass('waiting_ajax');
  $.ajax({
    type: "POST",
    url: "https://" + serverBaseUrl + serverRootPath + "/API/WS_Files.php",
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {

      if( msg.code == 0 ) {

        for( i = 0; i < objectsToDelete.length; i++ ) {
          var elm = objectsToDelete[i]["dataLocalId"];
          var isDeleted = $.inArray( elm, msg.response.filesDeleted ) >= 0 ? true: false;
          if(isDeleted){
            $(".directoryObj[data-local-id=" + elm + "]").removeClass("warning").addClass("success").delay( 2000 ).fadeOut();

          } else{
            console.log(msg.response.rawError);
            $(".directoryObj[data-local-id=" + elm + "]").removeClass("warning").addClass("danger").delay( 1000 );//.removeClass("danger");
          }

        }

      } else {
        console.log("ERROR");
        $("#directoryOpLoader").removeClass('waiting_ajax');
        return null;
      }
    },
    error: function(msg) {
      console.log("FATAL_ERROR");
      return null;
    },
    complete: function() {
      $("#directoryOpLoader").removeClass('waiting_ajax');
    }
  });
}

function DownloadFile(currentElement, currentObj){

  // var url = "https://" + serverBaseUrl + serverRootPath + "API/downloadFile.php"
	var url = "https://" + serverBaseUrl + serverRootPath + "API/downloadFile_bis.php"

	var form = document.createElement("form");
    form.target = "_blank";
    form.method = "POST";
    form.action = url;
    form.style.display = "none";

    for (var key in currentObj) {
        var input = document.createElement("input");
        input.type = "hidden";
        input.name = key;
        input.value = currentObj[key];
        form.appendChild(input);
    }

    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function PasteObject( action, currentObj ){
  var pasteDestination = null;

  if (GlobalPaste == null){
    console.log('nothing in memory');
    return;
  }

  if( action == 'pasteRoot' ){
    pasteDestination = GlobalCurrentDirectory + '/';
  } else {
    if (currentObj.objClass != 'dir'){
      pasteDestination = GlobalCurrentDirectory + '/';
    } else {
      pasteDestination = currentObj.completePath + '/';
    }
  }

  var dataJSON = {
    action: "copy_object",
    value: {
      source: {
	       name: GlobalPaste.name,
	       path: GlobalPaste.completePath
	       },
      destination: {
	       name: "",
	       path: pasteDestination
	      }
      }
  };

  $('#toolbarButton_Paste').addClass('disabled');
  $("#directoryOpLoader").addClass('waiting_ajax');
  $.ajax({
    type: "POST",
    url: "https://" + serverBaseUrl + serverRootPath + "/API/WS_Files.php",
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {

      if( msg.code == 0 ) {
        StartStop_CopyStatusEventListener();

      } else {
        console.log("ERROR");
        $('#toolbarButton_Paste').removeClass('disabled');
        $("#directoryOpLoader").removeClass('waiting_ajax');
        return null;
      }
    },
    error: function(msg) {
      console.log("FATAL_ERROR");
      return null;
    },
    complete: function() {
      $('#toolbarButton_Paste').removeClass('disabled');
      $("#directoryOpLoader").removeClass('waiting_ajax');
    }
  });
}

function ElementClicked(thisField){
  var elementLocalId = thisField.attr("data-local-id");
  var selectedObject = CacheCurrentDirectory['filesList'][elementLocalId];
  var elementType = selectedObject.objClass;

  if(SelectionMode == 0 && Object.keys(SelectedObjectArray).length <= 1){
    switch (elementType) {
      case 'file':
        FileClicked(selectedObject);
        break;
      case 'dir':
        UnselectAll();
        ForwardDirectory = [];
        SelectedObjectArray = [];
        LoadFileList(GlobalCurrentDirectory + "/" + selectedObject.name);
        break;
      default:
        console.log('pressed:' + selectedObject);
    }

  } else {

    ElementSelected(selectedObject, thisField);

  }
}

function ElementSelected(selectedObject, selectedElement){

  if(SelectionMode == 1){
    //shift
    // get the last selected element and object

    if(Object.keys(SelectedObjectArray).length == 0){
      SelectOneElement(selectedObject, selectedElement);
      return;
    }

    var currentObjSelected = SelectedObjectArray[ Object.keys(SelectedObjectArray)[Object.keys(SelectedObjectArray).length - 1] ];
    var currentElmSelected = $("tr[data-local-id='" + currentObjSelected.id +"']");

    while (true) {
      if(currentObjSelected.id < selectedObject.id){
        currentElmSelected = currentElmSelected.next();
      } else {
        currentElmSelected = currentElmSelected.prev();
      }
      currentObjSelected = CacheCurrentDirectory['filesList'][ currentElmSelected.attr('data-local-id') ];
      SelectOneElement(currentObjSelected, currentElmSelected);

      if(currentObjSelected == selectedObject){
        return;
      }
    }

  } else if (SelectionMode == 2) {
    //ctrl
    SelectOneElement(selectedObject, selectedElement);
  }


}

function SelectOneElement(selectedObject, selectedElement){
  var dataLocalId = selectedElement.attr("data-local-id");
  $('#toolbarButton_Delete').removeClass('disabled');

  if(!selectedElement.hasClass('rowSelected')) {
    SelectedObjectArray[ dataLocalId ] = selectedObject;

    selectedElement.addClass('rowSelected');
  } else {
    delete SelectedObjectArray[dataLocalId];
    selectedElement.removeClass('rowSelected');
  }
  dataLocalId = null;

}

function UnselectAll(){
  SelectionMode = 0;//null;
  SelectedObjectArray = [];
  $('.rowSelected').removeClass('rowSelected');
  $('#toolbarButton_Delete').addClass('disabled');

}

function FileClicked(file){

}

function BackClicked(){
  var prevDirectory = null;
  var directoryLevel = CountOcurrences(GlobalCurrentDirectory, '/');

  if(directoryLevel > 1){
    ForwardDirectory.push(GlobalCurrentDirectory);
    prevDirectory = GlobalCurrentDirectory.substring(0, GlobalCurrentDirectory.lastIndexOf("/") )
  } else {
    return;
  }

  LoadFileList(prevDirectory);
}

function ForwardClicked(){
  var tmpDir = ForwardDirectory[ForwardDirectory.length -1];
  ForwardDirectory.pop();
  LoadFileList(tmpDir);

}

function RenderCopyStatusData(data){

  var dataFileAttribute = ['sourceFileName', 'sourceFullPath', 'destinationFullPath'];//destinationPath
  var dataProgressAttribute = ['sentData', 'speed', 'eta'];

  var progressBarObj = $('#copyInfo_ProgressBar');
  var multiFuncButton = $('#abortCopy');
  var pasteButton = $('#toolbarButton_Paste');

  if(PrevCopyState == null && data.status != "RUNNING"){
    PrevCopyState = data.status;
    return;
  }

  switch (data.status) {
    case ("IDLE_ERROR"):

      progressBarObj.addClass("progress-bar-danger");
      progressBarObj.removeClass("active");

      if(PrevCopyState != data.status){
        multiFuncButton.html("Clear");
        multiFuncButton.addClass("btn-warning");
        multiFuncButton.removeClass("btn-danger");
        multiFuncButton.removeClass('disabled');
        pasteButton.removeClass('disabled');
      }
      break;
    case ("IDLE_OK"):
      progressBarObj.removeClass("progress-bar-danger");
      progressBarObj.removeClass("active");
      progressBarObj.addClass("progress-bar-success");

      progressBarObj.attr("style", 'width: 100%;');
      progressBarObj.attr("aria-valuenow", 100);
      $("#copyInfo_percentage").html('100%');
      $("#copyInfo_percentage_hidden").html('100%');

      $("#copyInfo_sentData").html(data.objectData.sourceFileSize);
      $("#copyInfo_speed").html(0);
      $("#copyInfo_eta").html('0:00:00');

      if(PrevCopyState != data.status){
        multiFuncButton.html("Clear");
        multiFuncButton.addClass("btn-warning");
        multiFuncButton.removeClass("btn-danger");
        multiFuncButton.removeClass('disabled');
        pasteButton.removeClass('disabled');
      }

      if(GlobalPaste != null && GlobalCurrentDirectory + "/" == data.objectData.destinationPath){
        LoadFileList(GlobalCurrentDirectory);
      }

      break;
    case ("RUNNING"):

      //caso in which is the first time this part is execuded (new copy)
      if(PrevCopyState != data.status){
        $('#copyInfoContainer').slideDown('fast');
        $('#copyInfo_noCopy').slideUp('fast');

        for( i = 0; i < dataFileAttribute.length; i++ ) {
          currentAttribute = dataFileAttribute[i];
          currentValue = data['objectData'][currentAttribute];
          $("#copyInfo_" +  currentAttribute).html(currentValue);
        }

        multiFuncButton.html("Cancel");
        multiFuncButton.removeClass("btn-warning");
        multiFuncButton.addClass("btn-danger");
        multiFuncButton.removeClass('disabled');
        pasteButton.addClass('disabled');
      }
      if(data.progress.sentData == null || data.progress.speed == null || data.progress.percentage == null){
        break;
      }

      for( i = 0; i < dataProgressAttribute.length; i++ ) {
        currentAttribute = dataProgressAttribute[i];
        currentValue = data['progress'][currentAttribute];
        $("#copyInfo_" +  currentAttribute).html(currentValue);
      }

      progressBarObj.removeClass("progress-bar-danger");
      progressBarObj.removeClass("progress-bar-success");
      progressBarObj.addClass("active");

      progressBarObj.attr("style", 'width: ' + data.progress.percentage + ';');
      progressBarObj.attr("aria-valuenow", data.progress.percentage);
      $("#copyInfo_percentage").html(data.progress.percentage);
      $("#copyInfo_percentage_hidden").html(data.progress.percentage);

      break;
    default:

  }

  PrevCopyState = data.status;
}