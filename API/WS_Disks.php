<?php
  header('Content-type: application/json');

  require_once("ORM_Util.php");
  require_once("ORM_DiskManager.php");

  session_start();
  clearstatcache();

  $UtilObject = new ORM_Util();
  $DiskManager = new ORM_DiskManager();

  $errorCode = 0;
  $errorMessage = "";
  $return_object = "OK";

  $JSON = $UtilObject->VerifyWsParameters($errorCode, $errorMessage);

  switch ($JSON->action) {

    case "disks_status":

      $return_object = $DiskManager->DisksStatus($JSON->value, $errorCode, $errorMessage);

      break;
    case "disk_mount":

      $return_object = $DiskManager->DiskMount($JSON->value, $errorCode, $errorMessage);

      break;
    case "disk_umount":

      $return_object = $DiskManager->DiskUmount($JSON->value, $errorCode, $errorMessage);

      break;

    case "get_known_disks":

      $return_object = $DiskManager->GetKnownDisks(false, null, $errorCode, $errorMessage);

      break;
    case "add_known_disk":

      $DiskManager->AddKnownDisk($JSON->value, $errorCode, $errorMessage);

      break;
    case "update_known_disk":

      $return_object = $DiskManager->UpdateKnownDisk($JSON->value, $errorCode, $errorMessage);

      break;
    case "delete_known_disk":

      $return_object = $DiskManager->DeleteKnownDisk($JSON->value, $errorCode, $errorMessage);

      break;
    default:

      $errorCode = -666;
      $errorMessage = "INVALID_REQUEST";

      break;

  }

  echo $UtilObject->ManageWebServiceResponse($return_object, $errorCode, $errorMessage);

?>
