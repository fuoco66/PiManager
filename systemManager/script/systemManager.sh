#!/bin/bash

if [ "$1" = "-p" ]
then

  PROCESS=$2
  PIDS=`ps cax | grep $PROCESS | grep -o '^[ ]*[0-9]*'`  
  
  res+=$(echo {)
  if [ -z "$PIDS" ];
  then
    # process is running
    res+=$(echo \"running\": false,)
    res+=$(echo \"pid\":null })
    
    # exit 1
  else
    # process is not running
    res+=$(echo \"running\": true,)
    processPids="["
    processPidsString="\""
    
    for PID in $PIDS; do
      processPids+=$(echo $PID, )
      processPidsString+=$(echo $PID, )
    done
    res+=$(echo \"pidString\": ${processPidsString: : -1}\", )
    res+=$(echo \"pid\": ${processPids: : -1}] })
    
  fi

  echo $res

elif [ "$1" = "-ssD" ]
then
  SERVICENAME=$2
  SERVICEACTION=$3

  service_cmd="service"
  cmdToRun="${service_cmd} ${SERVICENAME} ${SERVICEACTION}"
  eval "${cmdToRun}"
  
else
  echo "ERROR"
  exit 1
fi
