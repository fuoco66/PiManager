#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

char* concat(const char *s1, const char *s2){
    char *result = malloc(strlen(s1)+strlen(s2)+1);//+1 for the zero-terminator
    //in real code you would check for errors in malloc here
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

int main (int argc, char *argv[]){
   setuid (0);
   char* s1 = concat("/bin/bash /var/www/vhosts/pimanu.net/public/piManager/systemManager/script/systemManager.sh '", argv[1]);
   s1 = concat(s1, "' '");

   s1 = concat(s1, argv[2]);
   s1 = concat(s1, "' '");

   s1 = concat(s1, argv[3]);
   s1 = concat(s1, "'");

    system ( s1 );

    return 0;

}
