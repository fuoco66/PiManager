var RefreshDataEvent = null;
var RefreshRunning = false;
var PrevCopyState = null;
var inFileManager = false;
  
$( document ).ready(function() {

  if( $(location).attr('href').indexOf('fileManager') >= 0){
    inFileManager = true;
  }

  $('#abortCopy').on('click', function(){
    if( $('#abortCopy').html() == "Cancel"){
      StopCopyTask();
    } else{
      $('#copyInfoContainer').slideUp('fast');
      $('#copyInfo_noCopy').slideDown('fast');
    }
  });

  CheckCopySatus();

});

function StartStop_CopyStatusEventListener(){
  if(RefreshRunning){
    RefreshDataEvent.target.close();
    RefreshRunning = false;
  } else {
    $("#copyMonitor").addClass('open');

    RefreshRunning = true;
    var source = new EventSource("https://" + serverBaseUrl + serverRootPath + "/fileManager/events.php");
    source.addEventListener('message', function (event) {
      RefreshDataEvent = event;
      var obj = jQuery.parseJSON(event.data);

      if(obj.status != 'RUNNING'){
        StartStop_CopyStatusEventListener();
      }
    
    RenderCopyStatusData(obj);

    });
  }

}

function CheckCopySatus(){

  var dataJSON = {
    action: "copy_status"
  }

  $.ajax({
    type: "POST",
    url: "https://" + serverBaseUrl + serverRootPath + "/API/WS_Files.php",
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {

      if( msg.code == 0 ) {
        if(msg.response.isCopyRunning) {
          StartStop_CopyStatusEventListener();
        } else {
          RenderCopyStatusData(msg.response);
        }

      } else {
        console.log("ERROR");
        return null;
        // $("#directoryOpLoader").removeClass('waiting_ajax');
      }
    },
    error: function(msg) {
      console.log("FATAL_ERROR");
      return null;
      // $("#directoryOpLoader").removeClass('waiting_ajax');
    },
    complete: function() {
      updateRunning = false;

      // $("#directoryOpLoader").removeClass('waiting_ajax');
    }
  });

}

function RenderCopyStatusData(data){

  var dataFileAttribute = ['sourceFileName', 'sourceFullPath', 'destinationFullPath'];//destinationPath
  var dataProgressAttribute = ['sentData', 'speed', 'eta'];

  var progressBarObj = $('#copyInfo_ProgressBar');
  var multiFuncButton = $('#abortCopy');
  var pasteButton = null;
  if(inFileManager){
    pasteButton = $('#toolbarButton_Paste');
  }
  

  if(PrevCopyState == null && data.status != "RUNNING"){
    PrevCopyState = data.status;
    return;
  }

  switch (data.status) {
    case ("IDLE_ERROR"):

      progressBarObj.addClass("progress-bar-danger");
      progressBarObj.removeClass("active");

      if(PrevCopyState != data.status){
        multiFuncButton.html("Clear");
        multiFuncButton.addClass("btn-warning");
        multiFuncButton.removeClass("btn-danger");
        multiFuncButton.removeClass('disabled');
        if(inFileManager){
          pasteButton.removeClass('disabled');
        }
      }
      break;
    case ("IDLE_OK"):
      progressBarObj.removeClass("progress-bar-danger");
      progressBarObj.removeClass("active");
      progressBarObj.addClass("progress-bar-success");

      progressBarObj.attr("style", 'width: 100%;');
      progressBarObj.attr("aria-valuenow", 100);
      $("#copyInfo_percentage").html('100%');
      $("#copyInfo_percentage_hidden").html('100%');

      $("#copyInfo_sentData").html(data.objectData.sourceFileSize);
      $("#copyInfo_speed").html(0);
      $("#copyInfo_eta").html('0:00:00');

      if(PrevCopyState != data.status){
        multiFuncButton.html("Clear");
        multiFuncButton.addClass("btn-warning");
        multiFuncButton.removeClass("btn-danger");
        multiFuncButton.removeClass('disabled');
        if(inFileManager){
          pasteButton.removeClass('disabled');
        }        
      }

      if(inFileManager){
        if(GlobalPaste != null && GlobalCurrentDirectory + "/" == data.objectData.destinationPath){
          LoadFileList(GlobalCurrentDirectory);
        }
      }

      break;
    case ("RUNNING"):

      //caso in which is the first time this part is execuded (new copy)
      if(PrevCopyState != data.status){
        $('#copyInfoContainer').slideDown('fast');
        $('#copyInfo_noCopy').slideUp('fast');

        for( i = 0; i < dataFileAttribute.length; i++ ) {
          currentAttribute = dataFileAttribute[i];
          currentValue = data['objectData'][currentAttribute];
          $("#copyInfo_" +  currentAttribute).html(currentValue);
        }

        multiFuncButton.html("Cancel");
        multiFuncButton.removeClass("btn-warning");
        multiFuncButton.addClass("btn-danger");
        multiFuncButton.removeClass('disabled');
        
        if(inFileManager){
          pasteButton.addClass('disabled');
        }        
      }
      
      if(data.progress.sentData == null || data.progress.speed == null || data.progress.percentage == null){
        break;
      }

      for( i = 0; i < dataProgressAttribute.length; i++ ) {
        currentAttribute = dataProgressAttribute[i];
        currentValue = data['progress'][currentAttribute];
        $("#copyInfo_" +  currentAttribute).html(currentValue);
      }

      progressBarObj.removeClass("progress-bar-danger");
      progressBarObj.removeClass("progress-bar-success");
      progressBarObj.addClass("active");

      progressBarObj.attr("style", 'width: ' + data.progress.percentage + ';');
      progressBarObj.attr("aria-valuenow", data.progress.percentage);
      $("#copyInfo_percentage").html(data.progress.percentage);
      $("#copyInfo_percentage_hidden").html(data.progress.percentage);

      break;
    default:

  }

  PrevCopyState = data.status;
}

function StopCopyTask(){
  var dataJSON = {
    action: "copy_stop"
  };
  $('#abortCopy').addClass('disabled');
  // $("#directoryOpLoader").addClass('waiting_ajax');
  $.ajax({
    type: "POST",
    url: "https://" + serverBaseUrl + serverRootPath + "/API/WS_Files.php",
    data: JSON.stringify(dataJSON),
    dataType: "json",
    success: function(msg) {
      if( msg.code == 0 ) {
        StartStop_CopyStatusEventListener();
        CheckCopySatus();
      } else {
        $('#abortCopy').removeClass('disabled');
        $("#copyMonitor").addClass('open');
        console.log("ERROR");
        return null;
        // $("#directoryOpLoader").removeClass('waiting_ajax');
      }
    },
    error: function(msg) {
      $('#abortCopy').removeClass('disabled');
      console.log("FATAL_ERROR");
      return null;
      // $("#directoryOpLoader").removeClass('waiting_ajax');
    },
    complete: function() {

      // $("#directoryOpLoader").removeClass('waiting_ajax');
    }
  });
}