if [ "$1" = "-s" ]
then
  tac $2 |egrep -m 1 .

  if [ "$?" -eq "0" ]
  then
    return 0
  else
  	return 1
  fi

elif [ "$1" = "-sv" ]
then
  # tac $2 |egrep -m 1 .
  cat $2 |egrep @ -m 1

elif  [ "$1" = "-p" ]
then

  tail $2 | sed ':a;N;$!ba;s/\r/\n/g' | grep % | tail -1 | awk '{print $1}{print $2}{print $3}{print $4}'

else
  echo "ERROR"
  return 1
fi
